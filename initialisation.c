/** \file initialisation.c
 * \brief implementation of functions declared in implementation.h
 */

#include "initialisation.h"
#include "display.h"
#include "inifile.h"
#include "errors.h"
#include "utils.h"

#include <stdlib.h> 
#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <getopt.h>
#include <ctype.h>
#include <errno.h>

struct all_parameters default_parameters, current_parameters;

void usage()
{
    printf("Program usage is:\nserialtest [-h|--help] [-m|--mute] [-r|--rich] ");
    printf("[-i|--ini-file <filepath>] ");
    printf("([-t|--terminal <tty device> [(-s|--settings) <config string>]]) ");
    printf("([-d|--data <tty device> [(-s|--settings) <config string>]]) ");
    printf("([-e|--echo <tty device> [(-s|--settings) <config string>]]) ");
    printf("([-o|--termo <tty device> [(-s|--settings) <config string>]] ");
    printf("([-c|--control [<adress>,]<port>])\n"); //" ([-n|--nmea <tty device>])\n ");

    printf("\t-h|--help: print this message and quit\n");
    printf("\t-m|--mute: suppress status messages\n");
    printf("\t-i|--ini-file initialisation file containing persistent settings.\n");
    printf("Default value is \"serialtest.ini\"\n");
    printf("\t-r|--rich: enable rich display(with ncurses)\n");
    printf("\t-s|--settings: specify parameters of the serial port.\n");
    printf("Only parameters specified are baud rate, parity and byte\nsize. ");
    printf("Flow control and control characters will be as set on\nterminal");
    printf(" device. Format of config string is as follows:\n\t");
    printf("<baud rate><parity><character size><stopbits>, i.e.:\n\t9600n81\n");
    printf("Baud rates are standard constants up to 921600 (note that actual\n");
    printf("supported rate depends on device and its drivers). Parity is\n");
    printf("one of: even, odd, none, represented as e/E, o/O or n/N\n");
    printf("respectively. Bytesize is a digit from 5 to 8 and stop bits\n");
    printf("are a digit from 1 to 2.\n\t-t|--terminal: name of serial device to");
    printf("work on\n");
    printf("\t-e|--echo: name of echo device that repeatsdata from active device\n");
    printf("\t-d|--data: name of serial device connected to sensors block");
    printf("It is optional, and will not be used by default.\n");
    printf("\t-o|--termo: name of terminal device for interacting with termo controller\n");
    printf("For every device settings are taken from next configuration string\n");
    printf("\t-c|--control: TCP port on which the program  will be listening; 19019 by default\n");
    /* printf("\t-n|--nmea: name of device for interaction with Trimble GPS reciever\n"); */
    printf("\tOptionally, IP adress specification on which to listen\n");
}

void display_attribs(const struct termios *pterm)
{
    speed_t brate = 0;
    int bcount = 0;

    print_message(LOG_MESSAGE, "Displaying terminal attributes:\n");
    // retrieve baud rate

    {
        int i = 0;
        brate = cfgetospeed(pterm);
        for(; i < sizeof(baud_rates); ++i)
            if(brate == baud_bits[i])
            {
                print_message(LOG_MESSAGE, "Output speed: %d\n", baud_rates[i]);
                break;
            }
        brate = cfgetispeed(pterm);
        for(; i < sizeof(baud_rates); ++i)
            if(brate == baud_bits[i])
            {
                print_message(LOG_MESSAGE, "Input speed: %d\n", baud_rates[i]);
                break;
            }
    }
    /*  byte size and parity */
    switch(pterm->c_cflag & CSIZE)
    {
        case CS5: bcount = 5; break;
        case CS6: bcount = 6; break;
        case CS7: bcount = 7; break;
        case CS8: bcount = 8; break;
        default: print_message(LOG_MESSAGE, "Byte size: unknown\n"); break;
    }
    if(bcount > 0)
        print_message(LOG_MESSAGE, "Byte size: %d\n", bcount);
    if(pterm->c_cflag & CSTOPB)
        print_message(LOG_MESSAGE, "Stop bits: 2\n");
    else
        print_message(LOG_MESSAGE, "Stop bits: 1\n");

    if(pterm->c_cflag & PARENB)
    {
        if(!(pterm->c_cflag & PARODD))
            print_message(LOG_MESSAGE, "Parity: even\n");
        else
            print_message(LOG_MESSAGE, "Parity: odd\n");
    }
    else
        print_message(LOG_MESSAGE, "Parity: none\n");
}

void init_device_config(struct serial_config* pconfig)
{
    //strncpy(pconfig->device, "/dev/ttyS0", 20);
    memset(pconfig->device, 0, 20);
    pconfig->baud_rate_index = 15;
    pconfig->csize_flag = CS8;
    pconfig->stopbits_flag = 0;
    pconfig->parity_flag = 0;
    pconfig->has_value = false;
}

bool process_tcpport_string(char* arg, struct tcp_config* pport)
{
    if(!pport)
        return false;

    char *ip;
    char *port;
    struct addrinfo hints, *ai;
    int res;
    char *argcopy = strndup(arg, 50);
    ip = strtok(argcopy, ",");
    port = strtok(NULL, "");
    if(port == NULL)
    {
        port = ip;
        ip = NULL;
    }
    
    memset(&hints, 0, sizeof(hints));
    hints.ai_flags = AI_PASSIVE;
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;

    res = getaddrinfo(ip, port, &hints, &ai);
    if(res)
    {
        // could not addr info
        printf("Could not get adress info (error: %s); retrying with any address\n",
               gai_strerror(res));
        res = getaddrinfo(NULL, port, &hints, &ai);
        if(res)
        {
            printf("Failed to get adress info (error: %s)\n",
                   gai_strerror(res));
            free(argcopy);
            return false;
        }
    }

    // in case we already have addrinfo data
    if(pport->addr_info)
    {
        freeaddrinfo(pport->addr_info);
    }
    pport->addr_info = ai;
    if(ip == NULL)
    {
        strncpy(pport->ip_str, "0.0.0.0", 30);
    }
    else
    {
        strncpy(pport->ip_str, ip, 30);
    }
    strncpy(pport->port_str, port, 10);
    free(argcopy);
    return true;
}

void init_default_workmode(struct workmode* pmode)
{
    pmode->mute = false;
    pmode->rich = false;
    init_device_config(&pmode->active_port);
    // default settings for control port: any address, port number: 19019
    memset(&pmode->control_port, 0, sizeof(struct tcp_config));
    process_tcpport_string("19019", &pmode->control_port);

    init_device_config(&pmode->echo_port);
    init_device_config(&pmode->data_port);
    init_device_config(&pmode->termo_port);
    // make port non-working
    pmode->echo_port.device[0] = '\0';

    strcpy(pmode->ini_file, "aerostat.ini");
}

/** parse command line argument into data structure
 * \note argument is in global variable optarg
 * \returns help_flag value
 */
bool process_settings_string(const char* arg, struct serial_config* pconfig)
{
    if(arg == NULL)
    {
        printf("Settings string is missing!\n");
        return false;
    }
    int l = strlen(arg), i = 0;
    if(l < 4)
    {
        printf("Wrong configuration string format.\n");
        return true;
    }
    int r = atoi(arg);
    for(i = 0; i < sizeof(baud_rates); ++i)
        if(r == baud_rates[i])
            break;
    if(i == sizeof(baud_rates))
    {
        printf("Unknown baud rate: %d.\n", r);
        return true;
    }
    pconfig->baud_rate_index = i;
    // we go to end of config string, where parity|csize|stopbits are stored
    i = l - 3;
    switch(arg[i])
    {
    case 'n':
        pconfig->parity_flag = 0;
        break;
    case 'o':
        pconfig->parity_flag = PARENB | PARODD;
        break;
    case 'e':
        pconfig->parity_flag = PARENB;
        break;
    default:
        printf("Unknown parity.\n");
        return true;
    }
    switch(arg[i + 1])
    {
    case '5':
        pconfig->csize_flag = CS5;
        break;
    case '6':
        pconfig->csize_flag = CS6;
        break;
    case '7':
        pconfig->csize_flag = CS7;
        break;
    case '8':
        pconfig->csize_flag = CS8;
        break;
    default:
        printf("Unknown chracter size.\n");
        return true;
    }
    switch(arg[i + 2])
    {
    case '1':
        pconfig->stopbits_flag = 0;
        break;
    case '2':
        pconfig->stopbits_flag = CSTOPB;
        break;
    default:
        printf("Unknown stopbits.\n");
        return true;
    }
    pconfig->has_value = true;
    return false;
}

void print_port_settings(struct serial_config* pconfig)
{
    print_message(LOG_MESSAGE, "Baud rate: %d\n", baud_rates[pconfig->baud_rate_index]);
    print_message(LOG_MESSAGE, "Character size: %d\n", pconfig->csize_flag == CS5? 5:
                                   (pconfig->csize_flag == CS6? 6:
                                    (pconfig->csize_flag == CS7? 7: 8)));
    print_message(LOG_MESSAGE, "Parity: %s\n", pconfig->parity_flag&PARENB?
                            (pconfig->parity_flag&PARODD? "odd": "even"):
                            "none");
    print_message(LOG_MESSAGE, "Stopbits: %d\n", pconfig->stopbits_flag&CSTOPB? 2: 1); 
}

bool process_command_line(const int argc, char * const argv[], struct workmode* pmode)
{
#define HELP_VAL 'h'
#define MUTE_VAL 'm'
#define CONFIG_VAL 's'
#define TERMINAL_VAL 't'
#define CONTROL_VAL 'c'
#define RICHDISPLAY_VAL 'r'
#define INIFILE_VAL 'i'
#define ECHO_VAL 'e'
#define DATA_VAL 'd'
#define TERMO_VAL 'o'

    struct option long_options[] = {
           {"help", no_argument,	   NULL, HELP_VAL},
           {"mute", no_argument,	   NULL, MUTE_VAL},
           {"settings", required_argument, NULL, CONFIG_VAL},
           {"terminal", required_argument, NULL, TERMINAL_VAL},
           {"control", required_argument,  NULL, CONTROL_VAL},
           {"rich", no_argument, 	   NULL, RICHDISPLAY_VAL},
           {"ini-file", required_argument, NULL, INIFILE_VAL},
           {"echo", required_argument,	   NULL, ECHO_VAL},
           {"data", required_argument,	   NULL, DATA_VAL},
           {"termo", required_argument,	   NULL, TERMO_VAL},
           {0, 0, 0, 0}
    };
    const int active_port_f = 0,
        echo_port_f = 2,
        data_port_f = 4,
        termo_port_f = 8;
    int port_flag = 0; ///<- 0 for active port
                       ///<- 2 for echo port
                       ///<- 4 for data port
    bool help_flag = false;
    int parse_res = 0, longopt = 0;;
    opterr = 0;
    do
    {
        parse_res = getopt_long(argc, argv, ":hmri:c:t:e:s:d:l:o:", long_options, &longopt);
        switch(parse_res)
        {
        case HELP_VAL:
            help_flag = true;
            break;
        case MUTE_VAL:
            pmode->mute = true;
            break;
        case CONFIG_VAL:
            {
                if(port_flag == active_port_f)
                    process_settings_string(optarg, &pmode->active_port);
                else if(port_flag == echo_port_f)
                    process_settings_string(optarg, &pmode->echo_port);
                else if(port_flag == data_port_f)
                    process_settings_string(optarg, &pmode->data_port);
                else if(port_flag == termo_port_f)
                    process_settings_string(optarg, &pmode->termo_port);
                break;
            }
        case TERMINAL_VAL:
            strncpy(pmode->active_port.device, optarg, 20);
            port_flag = active_port_f;
            break;
        case CONTROL_VAL:
            process_tcpport_string(optarg, &pmode->control_port);
            break;
        case ECHO_VAL:
            strncpy(pmode->echo_port.device, optarg, 20);
            port_flag = echo_port_f;
            break;
        case TERMO_VAL:
            strncpy(pmode->termo_port.device, optarg, 20);
            port_flag = termo_port_f;
            break;         
        case DATA_VAL:
            strncpy(pmode->data_port.device, optarg, 20);
            port_flag = data_port_f;
            break;
        case RICHDISPLAY_VAL:
            pmode->rich = true;
            break;
        case INIFILE_VAL:
            strncpy(pmode->ini_file, optarg, 127);
            break;
        case ':':
            printf("Argument for option \'%c\' is missing.\n", optopt);
            help_flag = true;
            break;
        case '?':
            printf("Unknown option \'%c\'.\n", optopt);
            help_flag = true;
            break;
        }
    } while (parse_res != -1);
    if(optind < argc)
        printf("Values without argument are not recognized.\n");
    if(!(help_flag || pmode->mute))
    {
        printf("Active device: %s\n", pmode->active_port.device);
        printf("Data device: %s\n", pmode->data_port.device);
        if(strlen(pmode->echo_port.device) > 0)
            printf("Echo device: %s\n", pmode->echo_port.device);
        if(strlen(pmode->termo_port.device) > 0)
            printf("Termo device: %s\n", pmode->termo_port.device);
    }

    return !help_flag;
}

int set_attribs(const int fd, struct termios* pterm, const struct serial_config* pconfig)
{
    // set baud rate
    LOGPRINT("Setting baud rate to %d.\n", baud_rates[pconfig->baud_rate_index]);
    // change to raw mode
    LOGPRINT("Setting terminal to raw mode\n");
    pterm->c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR
                        | IGNCR | ICRNL | IXON);
    pterm->c_oflag &= OPOST;
    pterm->c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
    cfsetispeed(pterm, baud_bits[pconfig->baud_rate_index]);
    cfsetospeed(pterm, baud_bits[pconfig->baud_rate_index]);
    LOGPRINT("Set character size, parity and stop bit flags.\n");
    // set character size
    pterm->c_cflag &= ~CSIZE;
    pterm->c_cflag |= pconfig->csize_flag;
    // set parity
    pterm->c_cflag &= ~(PARENB | PARODD);
    pterm->c_cflag |= pconfig->parity_flag;
    // set stopbits
    pterm->c_cflag &= ~CSTOPB;
    pterm->c_cflag |= pconfig->stopbits_flag;

    // enable reading; ignore control lines
    pterm->c_cflag |= CREAD | CLOCAL;
    // disable flow control
    // pterm->c_cflag &= ~CRTSCTS;
    pterm->c_cc[VMIN] = 10;
    pterm->c_cc[VTIME] = 0;
    LOGPRINT("Flush terminal.\n");
    // flush terminal
    tcflush(fd, TCIOFLUSH);
    LOGPRINT("Apply parameters.\n");
    return tcsetattr(fd, TCSANOW, pterm);
}

int restore_attribs(const int fd, const struct termios* pterm)
{
    LOGPRINT("Restore parameters.\n");
    return tcsetattr(fd, TCSANOW, pterm);
}

struct serial_config configuration_ports[3];

void save_port_settings(pinifile_data inidb)
{
    const char* serial_port_names[] = {
        "Data_port",
        "Active_port",
        "Termo_port"
    };
    char buffer[20];
    char parity, csize;
    for(int i = 0; i < 3; ++i)
    {
        sprintf(buffer, "%s", configuration_ports[i].device);
        set_entry(inidb, serial_port_names[i], "Device", buffer);
        switch(configuration_ports[i].parity_flag)
        {
        case PARENB | PARODD:
            parity = 'o';
            break;
        case PARENB:
            parity = 'e';
            break;
        case 0:
        default:
            parity = 'n';
            break;
        }
        switch(configuration_ports[i].csize_flag)
        {
        case CS5:
            csize = '5';
            break;
        case CS6:
            csize = '6';
            break;
        case CS7:
            csize = '7';
            break;
        case CS8:
        default:
            csize = '8';
            break;
        }
        sprintf(buffer, "%d%c%c%d",
                baud_rates[configuration_ports[i].baud_rate_index],
                parity, csize, configuration_ports[i].stopbits_flag?2:1);
        set_entry(inidb, serial_port_names[i], "Settings", buffer);
    }
}

void load_settings(const char* config_file, bool verbose)
{
  
    const char* inifilename = config_file;
    if(!inifilename)
        inifilename = "serialtest.ini";
    pinifile_data inidb;
    int res = open_ini_file(inifilename, &inidb);
    init_default_parameters();
    if(!!res)
    {
        memcpy(&current_parameters, &default_parameters,
               sizeof(struct all_parameters));
        init_conversion_routines();

        LOGPRINT("Error#%d during in file reading (%s).\n",
                 errno, strerror(errno));
        LOGPRINT("Using default values:\nAxis 1 limits: %d %d\n"
             "Axis 2 limits: %d %d\n", -270, 270,
             -90, 90);
        /* for(int i = 0; i < 2; ++i) */
        /* { */
        /*     program_limits[i].limit_pos = pos_limits[i]; */
        /*     program_limits[i].limit_neg = neg_limits[i]; */
        /*     program_limits[i].speed_min = 0; */
        /*     program_limits[i].speed_max = spd_dscrt_to_degree(axis_max_speed[i]); */
        /* } */
        return;
    }
    char buffer[15];
    
    LOGPRINT("Fetching data from ini-file.\n");

    const char* conversions = "Settings";
    current_parameters.DPR = get_integer(inidb, conversions, "DPR",
                                         default_parameters.DPR);
    current_parameters.SPDMAXDEG = get_real(inidb, conversions, "SPDMAXDEG",
                                            default_parameters.SPDMAXDEG);
    current_parameters.SPDMAX = get_integer(inidb, conversions, "SPDMAX",
                                            default_parameters.SPDMAX);
    current_parameters.SPDMIN = get_integer(inidb, conversions, "SPDMIN",
                                            default_parameters.SPDMIN);
    init_conversion_routines();    

    // azimuth delta is in the same section
    current_parameters.azimuth_delta = get_real(inidb, conversions, "AZDELTA",
                                                default_parameters.azimuth_delta);
    current_parameters.elevation_delta = get_real(inidb, conversions, "ELDELTA",
                                                  default_parameters.elevation_delta);
    
    for(int i = 0; i < 2; ++i)
    {
        sprintf(buffer, "Coefficients%d", i);
        current_parameters.K1[i] = get_real(inidb, buffer, "K1", default_parameters.K1[i]);
        current_parameters.KS[i] = get_real(inidb, buffer, "KS", default_parameters.KS[i]);
        current_parameters.KK[i] = get_real(inidb, buffer, "KK", default_parameters.KK[i]);
        current_parameters.KI[i] = get_real(inidb, buffer, "KI", default_parameters.KI[i]);
    }

    for(int i = 0; i < 2; ++i)
    {
        sprintf(buffer, "Axis%d", i);
        current_parameters.axis_limits[i].limit_pos = get_real(inidb, buffer, "Positive",
                                                               default_parameters.axis_limits[i].limit_pos);
        current_parameters.axis_limits[i].limit_neg = get_real(inidb, buffer, "Negative",
                                                               default_parameters.axis_limits[i].limit_neg);
        current_parameters.axis_limits[i].speed_min = get_real(inidb, buffer, "SpdMin",
                                                               default_parameters.axis_limits[i].speed_min);
        current_parameters.axis_limits[i].speed_max = get_real(inidb, buffer, "SpdMax",
                                                               default_parameters.axis_limits[i].speed_max);
        /* axis_max_speed[i] = spd_degree_to_dscrt(program_limits[i].speed_max); */
    }
    for(int i = 0; i < 2; ++i)
    {
        sprintf(buffer, "Axis%dMan", i);
        current_parameters.axis_man_limits[i].speed_max = get_real(inidb, buffer, "SpdMax",
                                                                   default_parameters.axis_man_limits[i].speed_max);
        current_parameters.axis_man_limits[i].accel = get_real(inidb, buffer, "Acceleration",
                                                               default_parameters.axis_man_limits[i].accel);
        current_parameters.axis_man_limits[i].decel = get_real(inidb, buffer, "Deceleration",
                                                               default_parameters.axis_man_limits[i].decel);
    }    
    for(int i = 0; i < 2; ++i)
    {
        sprintf(buffer, "AxisSigns%d", i);
        current_parameters.SensorSigns[i] = get_integer(inidb, buffer, "S_SIGN", 1);
        if(current_parameters.SensorSigns[i] > 0) current_parameters.SensorSigns[i] = 1;
        else current_parameters.SensorSigns[i] = -1;
        current_parameters.AmplifierDirectionFlags[i] = get_integer(inidb, buffer, "A_SIGN", -1);
        if(current_parameters.AmplifierDirectionFlags[i] > 0) current_parameters.AmplifierDirectionFlags[i] = 1;
        else current_parameters.AmplifierDirectionFlags[i] = -1;
    }

    const char* serial_port_names[] = {
        "Data_port",
        "Active_port",
        "Termo_port"
    };
    for(int i = 0; i < 3; ++i)
    {
        init_device_config(&configuration_ports[i]);
        int res = get_string(inidb, serial_port_names[i], "Device", configuration_ports[i].device, 20);
        if(res > 0)
        {
            res = get_string(inidb, serial_port_names[i], "Settings", buffer, 15);
            if(res > 0)
            {
                process_settings_string(buffer, &configuration_ports[i]);
            }
        }
    }
    LOGPRINT("Retrieved values:\nAxis 1 limits: %f %f\n"
             "Axis 2 limits: %f %f\n",
             current_parameters.axis_limits[0].limit_pos,
             current_parameters.axis_limits[0].limit_neg,
             current_parameters.axis_limits[1].limit_pos,
             current_parameters.axis_limits[1].limit_neg);

    close_ini_file(inidb);
}

void save_settings(const char* config_file, bool verbose)
{
    const char* inifilename = config_file;
    if(!inifilename)
        inifilename = "serialtest.ini";
    pinifile_data inidb = create_inidb();
    char outbuffer[128], sectionbuffer[15], coefbuffer[20];

    for(int i = 0; i < 2; ++i)
    {
        sprintf(sectionbuffer, "Axis%d", i);
        sprintf(outbuffer, "%f", current_parameters.axis_limits[i].limit_pos);
        set_entry(inidb, sectionbuffer, "Positive", outbuffer);

        sprintf(outbuffer, "%f", current_parameters.axis_limits[i].limit_neg);
        set_entry(inidb, sectionbuffer, "Negative", outbuffer);

        sprintf(outbuffer, "%f", current_parameters.axis_limits[i].speed_min);
        set_entry(inidb, sectionbuffer, "SpdMin", outbuffer);

        sprintf(outbuffer, "%f", current_parameters.axis_limits[i].speed_max);
        set_entry(inidb, sectionbuffer, "SpdMax", outbuffer);
    }

    for(int i = 0; i < 2; ++i)
    {
        sprintf(sectionbuffer, "Axis%dMan", i);
        sprintf(outbuffer, "%f", current_parameters.axis_man_limits[i].speed_max);
        set_entry(inidb, sectionbuffer, "SpdMax", outbuffer);

        sprintf(outbuffer, "%f", current_parameters.axis_man_limits[i].accel);
        set_entry(inidb, sectionbuffer, "Acceleration", outbuffer);

        sprintf(outbuffer, "%f", current_parameters.axis_man_limits[i].decel);
        set_entry(inidb, sectionbuffer, "Deceleration", outbuffer);
    }    


    const char* conversions = "Settings";
    sprintf(outbuffer, "%d", current_parameters.DPR);
    set_entry(inidb, conversions, "DPR", outbuffer);
    sprintf(outbuffer, "%f", current_parameters.SPDMAXDEG);
    set_entry(inidb, conversions, "SPDMAXDEG", outbuffer);
    sprintf(outbuffer, "%d", current_parameters.SPDMAX);
    set_entry(inidb, conversions, "SPDMAX", outbuffer);
    sprintf(outbuffer, "%d", current_parameters.SPDMIN);
    set_entry(inidb, conversions, "SPDMIN", outbuffer);

    sprintf(outbuffer, "%d", current_parameters.azimuth_delta);
    set_entry(inidb, conversions, "AZDELTA", outbuffer);
    sprintf(outbuffer, "%d", current_parameters.elevation_delta);
    set_entry(inidb, conversions, "ELDELTA", outbuffer);

    for(int i = 0; i < 2; ++i)
    {
        sprintf(coefbuffer, "Coefficients%d", i);
        sprintf(outbuffer, "%f", current_parameters.K1[i]);
        set_entry(inidb, coefbuffer, "K1", outbuffer);
        sprintf(outbuffer, "%f", current_parameters.KS[i]);
        set_entry(inidb, coefbuffer, "KS", outbuffer);
        sprintf(outbuffer, "%f", current_parameters.KK[i]);
        set_entry(inidb, coefbuffer, "KK", outbuffer);
        sprintf(outbuffer, "%f", current_parameters.KI[i]);
        set_entry(inidb, coefbuffer, "KI", outbuffer);
    }
    for(int i = 0; i < 2; ++i)
    {
        sprintf(coefbuffer, "AxisSigns%d", i);
        sprintf(outbuffer, "%d", current_parameters.SensorSigns[i]>0?1:-1);
        set_entry(inidb, coefbuffer, "S_SIGN", outbuffer);
        sprintf(outbuffer, "%d", current_parameters.AmplifierDirectionFlags[i]>0?1:-1);
        set_entry(inidb, coefbuffer, "A_SIGN", outbuffer);
    }

    save_port_settings(inidb);
    LOGPRINT("Dumping settings into %s.\n", inifilename);
    dump_ini_file(inifilename, inidb);
    close_ini_file(inidb);
}

void finish_port_init(struct workmode* pmode)
{
    if(!pmode)
        return;
    if(!pmode->data_port.has_value && configuration_ports[0].has_value)
    {
        memcpy(&pmode->data_port, &configuration_ports[0], sizeof(struct serial_config));
    }

    if(!pmode->active_port.has_value && configuration_ports[1].has_value)
    {
        memcpy(&pmode->active_port, &configuration_ports[1], sizeof(struct serial_config));
    }

    if(!pmode->termo_port.has_value && configuration_ports[2].has_value)
    {
        memcpy(&pmode->termo_port, &configuration_ports[2], sizeof(struct serial_config));
    }
}

void init_default_parameters()
{
    // all parameters flag are set to false
    default_parameters.K1[0] = 100;
    default_parameters.K1[1] = 200;

    default_parameters.KS[0] = 50;
    default_parameters.KS[1] = 100;

    default_parameters.KK[0] = 0.5;
    default_parameters.KK[1] = 1;

    default_parameters.KI[0] = 0;
    default_parameters.KI[1] = 0;

    default_parameters.KZ2D = 1.f;

    default_parameters.DPR = 20000;
    default_parameters.SPDMAX = 17890;
    default_parameters.SPDMIN = 200;
    default_parameters.SPDMAXDEG = 6.0;

    default_parameters.axis_man_limits[0].speed_max = 3.0;
    default_parameters.axis_man_limits[0].accel = 1.0;
    default_parameters.axis_man_limits[0].decel = 2.0;
    default_parameters.axis_man_limits[1].speed_max = 3.0;
    default_parameters.axis_man_limits[1].accel = 1.0;
    default_parameters.axis_man_limits[1].decel = 2.0;
    const int pos_limits[] = { 270, 90 };
    const int neg_limits[] = { -270, -90 };

    for(int i = 0; i < 2; ++i)
    {
        default_parameters.axis_limits[i].limit_pos = pos_limits[i];
        default_parameters.axis_limits[i].limit_neg = neg_limits[i];
        default_parameters.axis_limits[i].speed_min = 0.f;
        default_parameters.axis_limits[i].speed_max = 4.f;

        default_parameters.SensorSigns[i] = 1;
        default_parameters.AmplifierDirectionFlags[i] = -1;
    }

    default_parameters.azimuth_delta = 0.f;
    default_parameters.elevation_delta = 0.f;
}
