/** \file errors.h
 * Error codes used for functions in the program
 */
#ifndef _ERROR_H_
#define _ERROR_H_

enum error_codes
{
    success = 0,
    invalid_parameter,
    allocation_failure,
    file_io,
    privileges,
    general_failure
};

#define WRAPSYSCALL(x) !x?success:general_failure

#endif //_ERROR_H_