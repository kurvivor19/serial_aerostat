#ifndef _OPERATIONS_H_
#define _OPERATIONS_H_

#include "initialisation.h"
#include "trkprotocol.h"
#include "trajectory.h"
#include "bupprotocol.h"

#include <stdint.h>
#include <stdbool.h>
#include <pthread.h>
#include <signal.h>

struct axis_speed
{
    /// current axis speeds, deg/sec
    float speed[2];
};

struct axis_position
{
    uint16_t status;
    float position[2];
};

/** Arguments to second thread
 */
struct work_thread
{
    /// id of the thread
    pthread_t wpid;
    /// Active port file descriptor
    int afd;
    /// Data port file descriptor
    int dfd;
    int32_t total;
};

/** Arguments to termo thread
 */
struct work_thread_termo
{
    /// id of the thread
    pthread_t wpid;
    /// Termo port file descriptor
    int tfd;
};

/** Arguments to trimble thread
 */
struct trimble_thread
{
    /// id of the thread
    pthread_t wpid;
    /// trimble port file descriptor
    int tfd;
};

/** Termination signal handler
 * \param signum signal number
 */
void finish(int signum);

void raisealarm(int sugnum);

void indata(int sugnum);

/** Interaction with sensors block
 * IO event driven loop that recieves data on state of devices,
 * calculates command (speeds of drivers) and sends it to the same port
 * \param[in] arg pointer to the \see work_thread structure
 */
void* threadfunc(void* arg);

/** Interaction with termo control
 * IO event driven loop that recieves data on the temperatures of the devices
 * updates state of the system
 * \param[in] arg pointer to the \see work_thread_termo structure
 */
void* threadfunc_termo(void* arg);

/** Update currently known state
 * data on current state is written to current_state[1]
 */
void update_outer_stat();

/** Update currently known state
 * data on current state are written in current_state[0] or not at all (in case it is locked, \see statelock)
 * element is avaiable
 */
void update_inner_stat(const struct drive_states* pstate);

/** Retrieve time in msec from start of the day */
uint32_t get_time();

/** Set time in msec from start of the day */
int set_time(uint32_t newtime);

/** Setup thread privileges and signal handlers
 * \returns 0 on success
 */
int thread_setup();

/** Determine axis flags
 */
void apply_limits(struct drive_states* pstate, const int32_t program_limits_pos[], const int32_t program_limits_neg[]); 

/** Recover next data packet from port
 * automatically fixes data misalignment
 * \param fd file descriptor of the input data port
 * \param pstate drive state structure that will be updated
 * \param counter data counter increased by amount of retrieved data
 * \param inbuffer buffer for read data
 * \param buffer_size size of a buffer
 * \returns true if there was packet retrieved
 */
bool parse_input_data(int fd, struct drive_states* pstate, int* counter, unsigned char* inbuffer, size_t buffer_size);

/** Update limits used by thread function cycle
 * Uses some global state variables:
 * \see limitslock for accessing new data
 * \see program_limits_flag is set to 0 if update was completed
 * \param[inout] state drive state, which has ts min speed updated
 * \param[out] program_limits_pos
 * \param[out] program_limits_neg
 */
void update_limits_inner(struct drive_states *state, int32_t *program_limits_pos, int32_t *program_limits_neg);

/** Calculate period for pulse output based on frequency
 * \param zprivod frequency
 * \returns period
 */
int16_t reverse_zprivod(int16_t zprivod);

/** Send speeds to BUP and recieve response
 * \param[inout] current_state state of the system
 * \param[inout] command data structure being sent to bup
 * \param[inout] alarm_counter number of times alarm sign was recieved on any of the drives
 * \param[in] afd file descriptor for BUP connection
 * \param[inout] inbuffer buffer for recieving BUP responses
 * \returns false if alarm was raised
 */ 
bool bup_interaction(struct drive_states* current_state,
                     struct bup_data_block_set* command,
                     int* alarm_counter, int afd,
                     unsigned char* inbuffer,
                     int inbuffer_size);

/** checks if any of the drives is in alarm state
 * \returns true if asny of ALM flags are set
 */
bool any_alarm(const struct drive_states* pstate);

/** Calculate speeds for axes during following the trajectory
 * \note depending on trajectory type, different axes are used
 * \param[in] points_short_buffer buffer with next 3 trajectory points (discreets)
 * \param[in] time short buffer times (in msec)
 * \param current_time time at current moment (msec)
 * \param[inout] pstate state of the drives
 */
void perform_tracking(const int32_t points_short_buffer[2*2],
                      int32_t time_short_buffer[3],
                      int32_t current_time,
                      struct drive_states* pstate);

/** Set state flags according to data recieved from BUP
 * \param[out] pstate state of the system that will be updated
 * \param flags flags that need to be evaluated
 * \note READY flags are set when 1
 * \note ALARM flags are set when 0
 */
void process_state_flags(struct drive_states* pstate, uint8_t flags);

/** Process and execute command from controlling machine
 * \param[in] data for recieved command
 * \param[out] output buffer for answer
 * \param outbuffer_size number of allocated bytes for the answer
 * \param pdata extra information
 * \returns true if it is time to save parameters
 */
bool process_incoming_command(const struct bup_data_block_header* input,
                              struct bup_data_block_header* output,
                              size_t outbuffer_size, struct work_thread* pdata);

/** Prepare for sending data on the state of the system
 * \param[in] pstate state to transmit
 * \param[out] pdata pointer to the data section of answer packet
 * \param data_mode current data transfer mode
 * \param ptemp[in] pointer to temperature data
 * \returns size of data to send
 */
int send_stat(const volatile struct drive_states* pstate, void* pdata, enum data_modes data_mode,
    struct termo_state *ptemp);

/** Calculate speed of the axis based on its previous position
 * \param pstate axis state
 * \param cur_msec current time in milliseconds from start of the day
 */
void compute_real_speed(struct axis_state* pstate, suseconds_t cur_msec);

struct shared_state
{
    sig_atomic_t done;			///< Flag set when program should terminate
    sig_atomic_t input;			///< Flag set for active thread when there are new data
    sig_atomic_t follow;		///< Set to 1 if we are in the autofollowing mode

    pthread_mutex_t limitslock;		///< Lock on program limits
    sig_atomic_t program_limits_flag;	///< Set when programs limit are changed

    pthread_mutex_t commandlock;	///< Lock on changing current command
    sig_atomic_t bup_command;		///< When not 0, contains command to be next executed

    pthread_mutex_t trajectorylock;	///< Lock on changing trajectory data
    /// Data on current trajectory
    struct trajectory_state current_traj_data;

    pthread_mutex_t settingslock;	///< Lock on saving settings data

    pthread_mutex_t statelock;		///< Lock on the state
    sig_atomic_t newstate;
    /** Sensors data on current drive state
     * First to elements are written by work thread
     * last is last data sent
     */
    struct drive_states current_state[2];
    /// Current temperatures
    struct termo_state current_temperature;

    struct axis_speed target_speed;	///< Target speeds for all axes
    /// Target positions for all axes
    struct axis_position target_position;

    enum direction_codes target_direction;
};

extern struct shared_state shs;

#endif // _OPERATIONS_H_
