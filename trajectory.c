/** \file trajectory.c
 * Handless allocation of trajectory buffer
 */

#include "trajectory.h"
#include "errors.h"

#include <stdlib.h>
#include <stddef.h>
#include <string.h>

struct trajectory_buffer
{
    int buffer_size;
    /// index of the point being written
    int cursor;
    struct trajectory_point* points;
};

int init_trajectory(struct trajectory_header* pheader)
{

    uint32_t allocation;
    if(pheader == NULL)
        return invalid_parameter;

    pheader->start_delta = 0;
    pheader->pimpl = malloc(sizeof(struct trajectory_buffer));
    if(pheader->pimpl == NULL)
        return allocation_failure;

    struct trajectory_buffer* pinner = pheader->pimpl;
    // TODO: vary the size of buffer
    pinner->buffer_size = MAX_MEMORY_SIZE;

    pinner->points = calloc(pinner->buffer_size, sizeof(struct trajectory_point));
    if(!pinner->points)
    {
        free(pheader->pimpl);
        pheader->pimpl = NULL;
        return allocation_failure;
    }
    pinner->cursor = 0;
    return 0;
}

int clean_trajectory_header(struct trajectory_header* pheader)
{
    if(pheader == NULL)
        return invalid_parameter;

    if(pheader->pimpl == NULL)
        return 0; ///<- nothing to clean, so success

    struct trajectory_buffer* pinner = pheader->pimpl;
    free(pinner->points);

    free(pinner);

    pheader->pimpl = NULL;

    return 0;
}

struct trajectory_point* get_point_inner(struct trajectory_buffer* pinner, int point_number)
{
    if(point_number < 0 || point_number >= pinner->buffer_size)
    {
        return NULL;
    }
    if(pinner->cursor < point_number)
    {
        pinner->cursor = point_number;
    }
    return pinner->points + point_number;
}

const struct trajectory_point* get_read_point(const struct trajectory_header* pheader, int point_number)
{
    if(pheader == NULL || pheader->pimpl == NULL)
        return NULL;

    struct trajectory_buffer* pinner = pheader->pimpl;
    if(point_number > pinner->cursor)
        return NULL;
    return get_point_inner(pinner, point_number);
}

struct trajectory_point* get_write_point(const struct trajectory_header* pheader, int point_number)
{
    if(pheader == NULL || pheader->pimpl == NULL)
        return NULL;

    struct trajectory_buffer* pinner = pheader->pimpl;
    return get_point_inner(pinner, point_number);
}

struct trajectory_point* get_next_point(const struct trajectory_header* pheader)
{
    if(pheader == NULL || pheader->pimpl == NULL)
        return NULL;

    struct trajectory_buffer* pinner = pheader->pimpl;
    return get_point_inner(pinner, pinner->cursor+1);
}

void reinit_trajectory(struct trajectory_header* pheader)
{
    if(pheader == NULL || pheader->pimpl == NULL)
        return;

    pheader->start_delta = 0;
    struct trajectory_buffer* pinner = pheader->pimpl;
    pinner->buffer_size = pheader->length;
    pinner->cursor = 0;
    memset(pinner->points, 0, sizeof(struct trajectory_point) * pinner->buffer_size);
}

int get_current_size(const struct trajectory_header* pheader)
{
    if(pheader == NULL || pheader->pimpl == NULL)
        return 0;

    return ((struct trajectory_buffer*)(pheader->pimpl))->cursor + 1;
}
