#include "display.h"
#include "utils.h"
#include "initialisation.h"
#include "global.h"

#include <ncurses.h>
#include <stdarg.h>

extern time_t day_start;

struct display_settings
{
    bool curses;
    bool verbose;
    int row_size, col_size;

    // TODO: change to ncurses windows
    WINDOW* ctrl_window;
    int ctrl_line;
    WINDOW* active_window;
    int active_line;
    WINDOW* log_window;
    int log_line;
    WINDOW* state_window;
    WINDOW* limit_window;
};

static const int SECTION_SIZE = 10;

struct display_settings settings;

void init_display(bool fullscreen, bool verbose)
{
    settings.curses = fullscreen;
    settings.verbose = verbose;
    if(fullscreen)
    {
        initscr();
	noecho();
	clear();
	getmaxyx(stdscr, settings.row_size, settings.col_size);

        if(verbose)
            settings.ctrl_window = newwin(SECTION_SIZE, settings.col_size/2 - 5, 0, 0);
        else
            settings.ctrl_window = newwin(0, 0, 0, 0);
	settings.ctrl_line = 0;

        if(verbose)
            settings.active_window = newwin(SECTION_SIZE, settings.col_size/2 - 5, 12, 0);
        else
            settings.active_window = newwin(0, 0, 0, 0);
	settings.active_line = 0;

        if(verbose)
            settings.log_window = newwin(SECTION_SIZE, settings.col_size/2 - 5, 24, 0);
        else
            settings.log_window = newwin(0, 0, 0, 0);
	settings.log_line = 0;

        if(verbose)
            settings.state_window = newwin(10, settings.col_size/2 + 5, 0, settings.col_size/2 - 5);
        else
            settings.state_window = newwin(10, settings.col_size, 0, 0);

        if(verbose)
            settings.limit_window = newwin(6, settings.col_size/2 + 5, 11, settings.col_size/2 - 5);
        else
            settings.limit_window = newwin(6, settings.col_size/2, 11, 0);

	attron(A_UNDERLINE);
	mvwprintw(settings.ctrl_window, 0, 5, "Control port:\n..................");
	mvwprintw(settings.active_window, 0, 5, "Active port:\n..................");
	mvwprintw(settings.log_window, 0, 5, "Log:\n..................");
	attroff(A_UNDERLINE);

    }
}

void deinit_display()
{
   if(settings.curses)
    {
        delwin(settings.ctrl_window);
        delwin(settings.log_window);
        delwin(settings.active_window);
	delwin(settings.state_window);
        delwin(settings.limit_window);
        endwin();

        settings.curses = false;
    }
}

void print_message_v(enum message_types type, char* format, va_list pars)
{
    if(!settings.curses)
    {
        if(settings.verbose)
            vprintf(format, pars);
        else if(type == CONTROL_MESSAGE)
            vprintf(format, pars);
    }
    else
    {
        WINDOW* cur_window;
        int* line;
	switch(type)
	{
	case CONTROL_MESSAGE:
	    cur_window = settings.ctrl_window;
	    line = &settings.ctrl_line;
	    break;
	case ACTIVE_MESSAGE:
	    cur_window = settings.active_window;
	    line = &settings.active_line;
	    break;
	case LOG_MESSAGE:
        default:
	    cur_window = settings.log_window;
	    line = &settings.log_line;
	    break;
	};
	wmove(cur_window, 1 + *line, 0);
	wclrtoeol(cur_window);
        vwprintw(cur_window, format, pars);
	*line = (*line + 1) % (SECTION_SIZE - 1);
	wmove(cur_window, 1 + *line, 0);
	wclrtoeol(cur_window);
	wprintw(cur_window, "..................");
    }
}

void print_log_message(char* format, ...)
{
    va_list pars;
    va_start(pars, format);
    print_message_v(LOG_MESSAGE, format, pars);
    va_end(pars);
}

void print_message(enum message_types type, char* format, ...)
{
    va_list pars;
    va_start(pars, format);
    print_message_v(type, format, pars);
    va_end(pars);
}

void refresh_display()
{
    if(settings.curses)
    {
        if(settings.verbose)
        {
            wnoutrefresh(settings.log_window);
            wnoutrefresh(settings.ctrl_window);
            wnoutrefresh(settings.active_window);     
        }
	wnoutrefresh(settings.state_window);
        wnoutrefresh(settings.limit_window);
        doupdate();
    }
}

void display_state(const struct drive_states* pdata,
                   const struct trajectory_state* pts,
                   enum data_modes data_mode,
                   const struct termo_state* ptermo)
{
    if(pdata == NULL)
        return;

    static char data_proc_string[64] = {0};
    const char * statestring = NULL;
    switch(pdata->operation_mode)
    {
    case mIdle:
        statestring = "Idle";
        break;
    case mMovePnt:
        statestring = "Moving by axis";
        break;
    case mWaitStartTime:
        statestring = "Waiting for trajectory start";
        break;
    case mTracking:
        statestring = "Tracking";
        break;
    case mNotReady:
        statestring = "Not ready";
        break;
    case mAlarm:
        statestring = "Alarm";
        break;
    default:
        statestring = "";
    };
    int hour, min, sec;
    hour = (pdata->time_sec - day_start) / 60;
    min = hour % 60;
    hour = hour / 60;
    sec = (pdata->time_sec - day_start) % 60;
    
    if(settings.curses)
    {
        wclear(settings.state_window);
	wmove(settings.state_window, 0, 0);
        wprintw(settings.state_window, "Time: %02d:%02d:%02d\n",
                hour, min, sec);
        // wprintw(settings.state_window, "Axis#|%7s|%7s|PPE|DPE|PNE|DNE|ALM|RDY\n", "Pos", "Speed");
        wprintw(settings.state_window, "Axis#|%7s|%7s|%7s|PPE|DPE|PNE|DNE|ALM|RDY\n", "Pos", "Targ", "Speed");
        for(int i = 0; i < 2; ++i)
        {
            // wprintw(settings.state_window, "%-5d|%7.2f|%7.2f|%-3s|%-3s|%-3s|%-3s|%-3s|%-3s\n",
            wprintw(settings.state_window, "%-5d|%7d|%7d|%7d|%-3s|%-3s|%-3s|%-3s|%-3s|%-3s\n",
                    i+1, //pos_dscrt_to_degree(pdata->axis[i].current_position),
                    //spd_dscrt_to_degree(pdata->axis[i].zprivod),
                    pdata->axis[i].current_position,
                    pdata->axis[i].target_position,
                    pdata->axis[i].zprivod,
                    pdata->axis[i].limit[1]? " X": "",
                    pdata->axis[i].endpoint[1]? " X": "",
                    pdata->axis[i].limit[0]? " X": "",
                    pdata->axis[i].endpoint[0]? " X": "",
                    pdata->axis[i].alarm? " X": "",
                    pdata->axis[i].ready? " X": "");
            /* wprintw(settings.state_window, "Axis %d position: %d; (%f deg.)\n", */
            /*         i+1, pdata->axis[i].current_position, pos_dscrt_to_degree(pdata->axis[i].current_position)); */
            /* wprintw(settings.state_window, "Target: %d\n", pdata->axis[i].target_position); */
            /* wprintw(settings.state_window, "Zprivod %d (%f deg/s)\n", */
            /*         pdata->axis[i].zprivod, spd_dscrt_to_degree(pdata->axis[i].zprivod)); */
        }
        wprintw(settings.state_window, "State: %s\n", statestring);
        if(mWaitStartTime == pdata->operation_mode)
        {
            int deci_temp = (pts->trajectory.start_time - day_start) * 10 + pts->trajectory.start_delta +\
                pts->trajectory.deciseconds;
            hour = deci_temp / 600;
            min = hour % 60;
            hour = hour / 60;
            sec = (deci_temp / 10) % 60;
            wprintw(settings.state_window, "Trajectory start time: %02d:%02d:%02d.%01d\n",
                    hour, min, sec, deci_temp % 10);
        }
        if(data_mode == dmRecieving)
        {
            int loaded = get_current_size(&pts->trajectory);
            sprintf(data_proc_string,  "Recieving trajectory: %.0f %% (%d / %d)",
                    100.0 * loaded / pts->trajectory.length, loaded, pts->trajectory.length);
        }
        else if(data_mode == dmSending)
        {
            int sent = pts->log_cursor;
            sprintf(data_proc_string, "Sending trajectory: %.0f %% (%d / %d)",
                    100.0 * sent / pts->log.length, sent, pts->log.length);
        }
        wprintw(settings.state_window, "%s", data_proc_string);

        if(ptermo != NULL)
        {
            for(int i = 0; i < 2; ++i)
            {
                if(!ptermo->sensor[i])
                    wprintw(settings.state_window, "Termo sensor %d offline\n", i);
                else
                    wprintw(settings.state_window, "Termo sensor %d: %d°%s\n",
                            i, ptermo->temperature[i], ptermo->heating[i]?", heating":"");
            }
        }


        wmove(settings.limit_window, 0, 0);
        wprintw(settings.limit_window, "Axis#|%7s|%7s|%7s|%7s\n", "PMin", "PMax", "SMax", "SMin");
        for(int i = 0; i < 2; ++i)
        {
            wprintw(settings.limit_window, "%-5d|%7.2f|%7.2f|%7.2f|%7.2f\n",
                    1 + i, current_parameters.axis_limits[i].limit_pos,
                    current_parameters.axis_limits[i].limit_neg,
                    current_parameters.axis_limits[i].speed_max,
                    current_parameters.axis_limits[i].speed_min);
        }

    }
    else
    {
        printf("Time: %02d:%02d:%02d\n", hour, min, sec);
        for(int i = 0; i < 2; ++i)
        {
            printf("Axis %d position: %d; (%f deg.)\n",
                    i+1, pdata->axis[i].current_position, pos_dscrt_to_degree(pdata->axis[i].current_position));
            printf("Target: %d\n", pdata->axis[i].target_position);
            printf("Zprivod %d (%f deg/s)\n",
                    pdata->axis[i].zprivod, spd_dscrt_to_degree(pdata->axis[i].zprivod));
        }
        printf("State: %s\n", statestring);
        if(mWaitStartTime == pdata->operation_mode)
        {
            int deci_temp = (pts->trajectory.start_time - day_start) * 10 + pts->trajectory.start_delta +\
                pts->trajectory.deciseconds;
            hour = deci_temp / 600;
            min = hour % 60;
            hour = hour / 60;
            sec = (deci_temp / 10) % 60;
            printf("Trajectory start time: %02d:%02d:%02d.%01d\n",
                   hour, min, sec, deci_temp % 10);
        }
        if(data_mode == dmRecieving)
        {
            int loaded = get_current_size(&pts->trajectory);
            printf("Recieving trajectory: %.0f %% (%d / %d)",
                   100.0 * loaded / pts->trajectory.length, loaded, pts->trajectory.length);
        }
        if(ptermo != NULL)
        {
            for(int i = 0; i < 2; ++i)
            {
                if(!ptermo->sensor[i])
                    printf("Termo sensor %d offline\n", i);
                else
                    printf("Termo sensor %d: %d°%s\n",
                           i, ptermo->temperature[i], ptermo->heating[i]?", heating":"");
            }
        }
 
    }
}

static char hex_string[256];

const char* make_hex_string(const unsigned char* buffer, size_t size)
{
    if(size > 64)
        
        size = 64;
    *hex_string = '\0';
    hexprint_s(buffer, size, hex_string);
    return hex_string;
}
