#include <sys/select.h>
#include <unistd.h>
#include <errno.h>

#include "bupprotocol.h"
#include "utils.h"

enum validation_codes validate_bup_data(const uint8_t* pblock, size_t buffer_size)
{
    const size_t header_size = sizeof(struct bup_data_block_header);
    if(*pblock != bup_block_start)
        return MISALIGNED;
    if(buffer_size < header_size)
        return PENDING_HEADER;
    const struct bup_data_block_header* pheader = (struct bup_data_block_header*)pblock;
    if(buffer_size < header_size + pheader->size)
        return PENDING_DATA;
    uint8_t crc = calculate_crc8(pblock + header_size, pheader->size);
    if(crc != pheader->crc)
        return CORRUPT;
    return VALID;
}

bool recieve_bup_data(uint8_t* buffer, size_t buffer_size, int fd, int* pcursor)
{
    if(fd < 0 || buffer == NULL)
        return false;
    fd_set active_fd;
    FD_ZERO(&active_fd);
    FD_SET(fd, &active_fd);
    // wait interval is set to half of a millisecond
    struct timeval wait_interval = {0, 1500};

    int cursor = 0;
    if(pcursor)
        *pcursor = cursor;
    // minimum valid packet would be header and 1 byte of data
    if(cursor + sizeof(struct bup_data_block_header) + 1 >= buffer_size)
        return false;
    int res = select(FD_SETSIZE, &active_fd, NULL, NULL, &wait_interval);
    while(res > 0 && FD_ISSET(fd, &active_fd))
    {
        do
        {
            res = read(fd, buffer + cursor, 1);
            if(res < 1)
                continue;
            cursor += res;
            if(pcursor)
                *pcursor = cursor;
            int valres = validate_bup_data(buffer, cursor);
            switch(valres)
            {
            case MISALIGNED:
                cursor = 0;
                continue;
            case PENDING_HEADER:
            case PENDING_DATA:
                if(cursor >= buffer_size)
                    return false;
                break;
            case VALID:
                return true;
            case CORRUPT:
                return false;
            }
        }while(res > 0);
        wait_interval.tv_sec = 0;
        wait_interval.tv_usec = 1500;
        res = select(FD_SETSIZE, &active_fd, NULL, NULL, &wait_interval);
    }
    return false;
}

bool write_bup_data(struct bup_data_block_header* pheader, int fd)
{
    if(pheader == NULL || fd < 0)
        return false;
    pheader->blockstart = bup_block_start;
    pheader->crc = calculate_crc8((uint8_t*)(pheader + 1), pheader->size);
    return 0 > TEMP_FAILURE_RETRY(write(fd, (void*)pheader, sizeof(struct bup_data_block_header) + pheader->size));
}
