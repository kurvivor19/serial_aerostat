/** \file inifile.h
 * Functions for reading configuration from disk
 * Configuration options have single lines
 * Comments start with ';'
 */

#ifndef _INIFILE_H_
#define _INIFILE_H_

typedef void* pinifile_data;

/** Open and parse an ini file
 * \param[in] fname name of file to read
 * \param[out] pinidb pointer to the variable for storing reference for
 * inifile data
 * \returns 0 on success
 */
int open_ini_file(const char* fname, pinifile_data* pinidb);

int get_integer(const pinifile_data inidb, const char* section, const char* entry, int default_value);
double get_real(const pinifile_data inidb, const char* section, const char* entry, double default_value);
int get_string(const pinifile_data inidb, const char* section, const char* entry, char* recieving_buffer, int max_buffer_size);
/** Free memory used by ini file database
 * \param[in] inidb ini file database from open_ini_file
 */
void close_ini_file(pinifile_data inidb);

/** Create ini file database */
pinifile_data create_inidb();

/** Add or change value in ini file database
 * If there is no specified section or entry in database, they will be created
 * \param inidb ini file database
 * \param[in] section name of a section
 * \param[in] entry name of an entry
 * \param[in] value new value of entry in string form
 */
void set_entry(pinifile_data inidb, const char* section, const char* entry, const char* value);

/** Dump ini file database on disc
 * \param fname name of a file to dump into
 * \param inidb ini file database
 * \returns 0 on success
 */
int dump_ini_file(const char* fname, pinifile_data inidb);

#endif // _INIFILE_H_
