#include "operations.h"
#include "global.h"
#include "display.h"

#include <stddef.h>
#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <signal.h>
#include <stdlib.h>

extern volatile sig_atomic_t input;

bool parse_input_data(int fd, struct drive_states* pstate, int* counter, unsigned char* inbuffer, size_t buffer_size)
{
    // read everything that comes through
    int res = read(fd, inbuffer, buffer_size);
    // calculate changes
    for(int i = 0; i < 3; ++i)
    {
        // if(pstate->operation_mode != mIdle)
        //     print_message(ACTIVE_MESSAGE, "Axis %d: privod = %d, pos = %d\n",
        //     i, pstate->axis[i].zprivod, pstate->axis[i].current_position);
        
        float diff = round(pstate->axis[i].zprivod / 5000.f);
        pstate->axis[i].current_position += diff;

    }
    input = res > 0;
    return input;
}
