#include "inifile.h"
#include "errors.h"

#include <stdio.h>
#include <string.h>
#include <stddef.h>
#include <stdlib.h>
#include <ctype.h>

struct inifile_entry
{
    const char* entry_name;
    const char* entry_value;
    struct inifile_entry* next_entry;
};

struct inifile_section
{
    const char* section_name;
    struct inifile_entry* first_entry;
    struct inifile_section* next_section;
};

struct inifile_data
{
    struct inifile_section* first_section;
};

void close_ini_file(pinifile_data inidb)
{
    struct inifile_data* pinidb = (struct inifile_data*)inidb;
    if(pinidb == NULL)
        return;

    struct inifile_section* pnext_s = NULL, * pcur_s = NULL;
    struct inifile_entry* pnext_e = NULL, * pcur_e = NULL;

    pcur_s = pinidb->first_section;
    while(pcur_s != NULL)
    {
        pnext_s = pcur_s->next_section;

        pcur_e = pcur_s->first_entry;
        while(pcur_e != NULL)
        {
            pnext_e = pcur_e->next_entry;
            if(pcur_e->entry_name != NULL)
                free((void*)pcur_e->entry_name);
            if(pcur_e->entry_value != NULL)
                free((void*)pcur_e->entry_value);
            free((void*)pcur_e);
            pcur_e = pnext_e;
        }
        if(pcur_s->section_name != NULL)
            free((void*)pcur_s->section_name); 
        free((void*)pcur_s);
        pcur_s = pnext_s;
    }
    free((void*)pinidb);
}

enum parser_states
{
    comment = 0,
    linestart,
    sectionentry,
    entryname,
    entryvalue,
    states_number
};

struct parser_state
{
    struct inifile_data context;
    struct inifile_section* section_cursor;
    struct inifile_entry* entry_cursor;
    char buffer[255];
    int buffer_cursor;
};

typedef enum parser_states (*ini_file_state_transition)(char next_char, struct parser_state* pastate);

enum parser_states comment_state(char next_char, struct parser_state* pastate)
{
    if(next_char == '\n')
    {
        pastate->buffer_cursor = 0;
        return linestart;
    }
    return comment;
}

enum parser_states linestart_state(char next_char, struct parser_state* pastate)
{
    if(next_char == '\n')
    {
        pastate->buffer_cursor = 0;
    return linestart;
    }
    if(next_char == ';')
    {
        pastate->buffer_cursor = 0;
        return comment;
    }
    if(next_char == ']' || next_char == '=')
    {
        // this is error, discard the string
        pastate->buffer_cursor = 0;
        return comment;
    }
    if(isspace(next_char))
        return linestart;
    if(next_char == '[')
    {
        pastate->buffer_cursor = 0;
    return sectionentry;
    }
    // in all other cases, start of entry
    if(pastate->section_cursor == NULL)
    {
        //entries without section are not considered
        pastate->buffer_cursor = 0;
    return comment;
    }
    pastate->buffer[pastate->buffer_cursor++] = next_char;
    return entryname;    
}

enum parser_states sectionentry_state(char next_char, struct parser_state* pastate)
{
    if(next_char == '\n')
    {
        // this is an error; data are discarded
        pastate->buffer_cursor = 0;
    return linestart;
    }
    if(next_char == '[' || next_char == ';')
        return comment;

    if(next_char != ']')
        pastate->buffer[pastate->buffer_cursor++] = next_char;
    // protection from buffer overflow
    if(next_char == ']' || pastate->buffer_cursor > 254)
    {
        // this is section name's end
        // if we have non-empty name, create new section
        if(pastate->buffer_cursor > 0)
        {
            // prepare next section
            if(pastate->section_cursor == NULL)
            {
                // make first section
                pastate->context.first_section = calloc(1, sizeof(struct inifile_section));
                pastate->section_cursor = pastate->context.first_section;
            }
            else
            {
                pastate->section_cursor->next_section = calloc(1, sizeof(struct inifile_section));
                pastate->section_cursor = pastate->section_cursor->next_section;
            }
            pastate->entry_cursor = NULL;
            char* temp = malloc(pastate->buffer_cursor + 1);
            memcpy(temp, pastate->buffer, pastate->buffer_cursor);
            temp[pastate->buffer_cursor] = '\0';
            pastate->section_cursor->section_name = temp;
        }
        // discard the rest of the string
        return comment;
    }
    
    return sectionentry;

}

enum parser_states entryname_state(char next_char, struct parser_state* pastate)
{
    if(next_char == '\n')
    {
        // this is an error; data are discarded
        pastate->buffer_cursor = 0;
        return linestart;
    }
    if(next_char == ';' || next_char == '[' || next_char == ']')
    {
        // this is an error; data are discarded
        pastate->buffer_cursor = 0;
        return comment;
    }
    if(next_char != '=')
        pastate->buffer[pastate->buffer_cursor++] = next_char;
    // protection from buffer overflow
    if(pastate->buffer_cursor > 254)
    {
        // too long, discard everything
        pastate->buffer_cursor = 0;
        return comment;
    }

    if(next_char == '=')
    {
        // make new entry
        if(pastate->entry_cursor == NULL)
        {
            // make first entry
            pastate->section_cursor->first_entry = calloc(1, sizeof(struct inifile_entry));
            pastate->entry_cursor = pastate->section_cursor->first_entry;
        }
        else
        {
            pastate->entry_cursor->next_entry = calloc(1, sizeof(struct inifile_entry));
            pastate->entry_cursor = pastate->entry_cursor->next_entry;
        }
        char* temp = malloc(pastate->buffer_cursor + 1);
        memcpy(temp, pastate->buffer, pastate->buffer_cursor);
        temp[pastate->buffer_cursor] = '\0';
        pastate->entry_cursor->entry_name = temp;
        pastate->buffer_cursor = 0;
        return entryvalue;
    }
    return entryname;
}

enum parser_states entryvalue_state(char next_char, struct parser_state* pastate)
{
    enum parser_states res = entryvalue;
    if(next_char == '\n')
    {
        res = linestart;
    }
    else if(next_char == ';')
    {
        res = comment;
    }
    else
    {
        pastate->buffer[pastate->buffer_cursor++] = next_char;
    if(pastate->buffer_cursor > 254)
        res = comment;
    }
    if(res != entryvalue)
    {
        char* temp = malloc(pastate->buffer_cursor + 1);
        memcpy(temp, pastate->buffer, pastate->buffer_cursor);
        temp[pastate->buffer_cursor] = '\0';
        pastate->entry_cursor->entry_value = temp;
        pastate->buffer_cursor = 0;
    }
    
    return res;

}

int open_ini_file(const char* fname, pinifile_data* pinidb)
{
    if(fname == NULL || pinidb == NULL)
        return invalid_parameter;

    FILE* fstream = fopen(fname, "r");
    if(!fstream)
        return file_io;

    struct parser_state context;
    memset(&context, 0, sizeof(struct parser_state));
    int character = 0, state_index = linestart;
    ini_file_state_transition parse_functions_table[] = {
        comment_state,
        linestart_state,
        sectionentry_state,
        entryname_state,
        entryvalue_state
    };
    do
    {
        character = fgetc(fstream);
        if(character != EOF)
        {
            state_index = parse_functions_table[state_index](character, &context);
        }
    } while(!(feof(fstream) || ferror(fstream)));
    fclose(fstream);

    *pinidb = malloc(sizeof(struct inifile_data));
    // here we are copying a pointer, yes
    memcpy(*pinidb, &context.context, sizeof(struct inifile_data));

    return success;    
}

const char* get_value(const pinifile_data inidb, const char* section, const char* entry)
{
    struct inifile_data* pinidb = (struct inifile_data*)inidb;
    if(pinidb == NULL || section == NULL || entry == NULL)
        return NULL;

    struct inifile_section* pcur_s = NULL;
    struct inifile_entry* pcur_e = NULL;

    pcur_s = pinidb->first_section;
    while(pcur_s != NULL)
    {
        pcur_e = pcur_s->first_entry;

        if(!strcmp(section, pcur_s->section_name))
            while(pcur_e != NULL)
            {
                if(!strcmp(entry, pcur_e->entry_name))
                    return pcur_e->entry_value;
                pcur_e = pcur_e->next_entry;
            }
        pcur_s = pcur_s->next_section;
    }
    return NULL;
}

int get_string(const pinifile_data inidb, const char* section, const char* entry, char* recieving_buffer, int max_buffer_size)
{
    const char* value = get_value(inidb, section, entry);
    if(!value || !recieving_buffer)
        return -1;
    strncpy(recieving_buffer, value, max_buffer_size);
    recieving_buffer[max_buffer_size - 1] = 0;
    return strlen(recieving_buffer);
}

int get_integer(const pinifile_data inidb, const char* section, const char* entry, int default_value)
{
    const char* evalue = get_value(inidb, section, entry);
    if(!evalue)
        return default_value;
    return strtol(evalue, NULL, 10);
}

double get_real(const pinifile_data inidb, const char* section, const char* entry, double default_value)
{
    const char* evalue = get_value(inidb, section, entry);
    if(!evalue)
        return default_value;
    return strtod(evalue, NULL);

}

pinifile_data create_inidb()
{
    return calloc(1, sizeof(struct inifile_data));
}

void set_entry(pinifile_data inidb, const char* section, const char* entry, const char* value)
{
    struct inifile_data* pinidb = (struct inifile_data*)inidb;
    if(pinidb == NULL || section == NULL || entry == NULL || value == NULL)
        return;

    struct inifile_section* target_section = NULL,* section_cursor = NULL;
    // find section
    section_cursor = pinidb->first_section;
    while(section_cursor != NULL)
    {
        if(!strcmp(section, section_cursor->section_name))
        {
            target_section = section_cursor;
            break;
        }
        if(NULL == section_cursor->next_section)
            break;
        section_cursor = section_cursor->next_section;
    }
    if(target_section == NULL)
    {
         // make new section with given name
         if(section_cursor == NULL)
         {
             // make first section
             pinidb->first_section = calloc(1, sizeof(struct inifile_section));
             target_section = pinidb->first_section;
         }
         else
         {
             section_cursor->next_section = calloc(1, sizeof(struct inifile_section));
             target_section = section_cursor->next_section;
         }
         char* sname = malloc(strlen(section) + 1);
         target_section->section_name = strcpy(sname, section);
    }

    struct inifile_entry* target_entry = NULL,* entry_cursor = NULL;
    // find entry in found section
    entry_cursor = target_section->first_entry;
    while(entry_cursor != NULL)
    {
        if(!strcmp(entry, entry_cursor->entry_name))
        {
            target_entry = entry_cursor;
            break;
        }
        if(entry_cursor->next_entry == NULL)
            break;
        entry_cursor = entry_cursor->next_entry;
    }
    if(target_entry == NULL)
    {
        // create new entry with given name
        if(entry_cursor == NULL)
        {
            // make first entry
            target_section->first_entry = calloc(1, sizeof(struct inifile_entry));
            target_entry = target_section->first_entry;
        }
        else
        {
            entry_cursor->next_entry = calloc(1, sizeof(struct inifile_entry));
            target_entry = entry_cursor->next_entry;
        }
        char* ename = malloc(strlen(entry) + 1);
        target_entry->entry_name = strcpy(ename, entry);
    }
    if(target_entry->entry_value != NULL)
        free((void*)target_entry->entry_value);

    char* eval = malloc(strlen(value) + 1);
    target_entry->entry_value = strcpy(eval, value);
}

int dump_ini_file(const char* fname, pinifile_data inidb)
{
    struct inifile_data* pinidb = (struct inifile_data*)inidb;
    if(fname == NULL || pinidb == NULL)
        return invalid_parameter;
    FILE* fstream = fopen(fname, "w");
    if(!fstream)
        return file_io;

    struct inifile_section* section_cursor = pinidb->first_section;
    while(section_cursor != NULL)
    {
        fprintf(fstream, "[%s]\n", section_cursor->section_name);
        struct inifile_entry* entry_cursor = section_cursor->first_entry;
        while(entry_cursor != NULL)
        {
            fprintf(fstream, "%s=%s\n", entry_cursor->entry_name, entry_cursor->entry_value);
            entry_cursor = entry_cursor->next_entry;
        }
        section_cursor = section_cursor->next_section;
    }
    fclose(fstream);
    return success;
}
