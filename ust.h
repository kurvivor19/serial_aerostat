/** \file ust.h
 * \brief Definition of ust file structure
 * \see USTSTRUC.DOC
 */

#ifndef _UST_H_
#define _UST_H_

#include <stdint.h>

/// UST file header
struct TUSTHeader
{
    /// Name of cosmic apparatus
    char SatName[32];
    /// Date-time of trajectory start in GMT
    int16_t Year;
    int16_t Month;
    int16_t Day;
    int16_t Hour;
    int16_t Min;
    int16_t Sec;
    /// Time interval between trajectory points
    float TimeStep;
    /// Number of points in trajectory
    int16_t NumOfPoints;
    unsigned char Reserv[12];
    int16_t ExtraBytes;
};

/** Single element of trajectory array
 * Elements were calculated by following formulas:
 * Az = (int16_t)(floor(fAz * 8192.0 / 45.0)); <-- Azimuth
 * El = (int16_t)(floor(fEl * 8192.0 / 45.0)); <-- Elevation
 */
struct TUSTPoint
{
    int16_t Az;
    int16_t El;
};


#endif // _UST_H_