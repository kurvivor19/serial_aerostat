/** \file bupprotocol.h
 * Contains strucures of the protocol used to retrieve
 * servoamplifier states and send commands to device
 * control block (BUP)
 */

#ifndef _BUPPROTOCOL_H_
#define _BUPPROTOCOL_H_

#include <stdint.h>
#include <string.h>
#include <stdbool.h>

static const uint8_t bup_block_start = 0xAC;

struct __attribute__ ((packed)) bup_data_block_header
{
    uint8_t blockstart;	///< must be equal to \see bup_block_start
    uint8_t size;	///< size of data in the packet, 0..252 bytes
    uint8_t crc;	///< crc=8 checksum
};

enum __attribute__ ((packed)) bup_command_codes
{
    CMD_SET = 0x01
};

enum __attribute__ ((packed)) bup_directive_flags
{
    CMDP_OUT0 = 0x01,		///< 1 for output port 0 to be on
    CMDP_OUT1 = 0x02,		///< 1 for output port 1 to be on,
    CMDP_CH0_USEA = 0x10,	///< 1 for channel 0 to use exit A, 0 to use exit B
    CMDP_CH1_USEA = 0x20,	///< 1 for channel 1 to use exit A, 0 to use exit B
    CMDP_CH2_USEA = 0x40,	///< 1 for channel 2 to use exit A, 0 to use exit B
    CMDP_CH3_USEA = 0x80,	///< 1 for channel 3 to use exit A, 0 to use exit B
};

/** Data transmitted with CMD_SET command
 */
struct __attribute__ ((packed))  bup_data_block_set
{
    uint8_t cmd_code;		///< Command code, uqual to CMD_SET
    uint8_t cmd_flags;		///< Combination of \see bup_directive_flags
    uint16_t cmd_freq[4];	///< frequency of channels (Hz)
};

enum __attribute__ ((packed)) bup_status_flags
{
    CMDP_IN0 = 0x01,
    CMDP_IN1 = 0x02,
    CMDP_IN2 = 0x04,
    CMDP_IN3 = 0x08,
    CMDP_IN4 = 0x10,
    CMDP_IN5 = 0x20,
    CMDP_IN6 = 0x40,
    CMDP_IN7 = 0x80
};

/** Data recieved as a response for CMD_SET command
 */
struct __attribute__ ((packed)) bup_data_block_set_response
{
    uint8_t cmd_flags;
    uint8_t reserve;
};

enum validation_codes
{
    VALID,
    MISALIGNED,
    PENDING_HEADER,
    PENDING_DATA,
    CORRUPT
};

/** Validate data as a data from bup
 * Validate data pointed by pblock as valid data for BUP interaction
 * \param[in] pblock incoming data; points to the start of a header
 * \param buffer_size size of buffer being processed
 * \returns appropriate validation code
 */
enum validation_codes validate_bup_data(const uint8_t* pblock, size_t buffer_size);

/** Recieve a packet from given file descriptor
 * \note this function waits for data
 * \param buffer reciever for the packet; on successful execution packet is in there
 * \param buffer_size size of a given buffer
 * \param fd file descriptor to read from
 * \param cursor number of recieved bytes
 * \returns true if valid data was recieved
 */
bool recieve_bup_data(uint8_t* buffer, size_t buffer_size, int fd, int* pcursor);

/** Send packet
 * \param pheader pointer to the packet data
 * \note only length of data must be set
 * \note after header, there must be data, in continous memory
 * \param fd file descriptor to write to
 * \returns trus if data was succesfully sent
 */
bool write_bup_data(struct bup_data_block_header* pheader, int fd);

#endif //_BUPPROTOCOL_H_
