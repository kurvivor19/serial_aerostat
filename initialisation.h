/** \file initialisation.h
 * \brief types and function prototypes for work with command line
 */
#ifndef _INITIALISATION_H_
#define _INITIALISATION_H_

#include <sys/types.h>
#include <sys/socket.h>
#include <stdbool.h>
#include <termios.h>
#include <stddef.h>
#include <time.h>
#include <stdint.h>
#include <netdb.h>

struct serial_config
{
    bool has_value;
    /** Serial device name
     * \dev\ttyS0 by default
     */
    char device[20];
    /** Index of baud rate in corresponding table
     * 15 by default
     */
    int baud_rate_index;
    /** Flag for character size mask
     * CS8 by default
     */
    tcflag_t csize_flag;
    /** Flag for stop bits
     * 0 by default (1 stop bit)
     */
    tcflag_t stopbits_flag;
    /** Flag for parity
     * 0 (none) by default
     */
    tcflag_t parity_flag;
};

struct tcp_config
{
    char ip_str[30];
    char port_str[10];
    struct addrinfo* addr_info;

    int socket;
    int file;
};

/// Describes settings the program works with
struct workmode
{
    /** Supress output to calling terminal
     * false by default
     */
    bool mute;
    /** Enable rich display (using ncurses library)
     * false by default
     */
    bool rich;
    /** Configuration of active serial port
     * Input/output for this port is part of the main cycle
     */
    struct serial_config active_port;
    /** Configuration of control port
     * this port is used for recieving commands and transmitting current state
     * to remote PC
     */
    struct tcp_config control_port;
    /** Configuration of data serial port
     * this port is for retrieving sensors data
     */    
    struct serial_config data_port;
    /** Configuration of the echo serial port
     * debug purposes only
     * currently is used for sending counter byte from sensors data
     */
    struct serial_config echo_port;
    /** Configuration of the port for termo controller
     * this port is used to retrieve data on temperature
     */
    struct serial_config termo_port;

    char ini_file[128];
};


/// Baud rate flags table
static const tcflag_t baud_bits[] = {
        B0, B50, B75, B110, B134, B150, B200, B300, B600,
        B1200, B1800, B2400, B4800, B9600, B19200, B38400,
        B57600, B115200, B230400, B460800, B921600
};

/// Baud rate values table
static const speed_t baud_rates[] = {
       0, 50, 75, 11 , 134, 150, 200, 300, 600,
       1200, 1800, 2400, 4800, 9600, 19200, 38400,
       57600, 115200, 230400, 460800, 921600
};

/// current day start in seconds from EPOCH
extern time_t day_start;
/// current day end in seconds from EPOCH
extern time_t day_end;

static const time_t day_len = 24 * 3600;

/**
 * \brief Process command line arguments
 * Process command line parameters and fill provided workmode struct
 * \note no sanity checks are performed
 * \param argc number of command line arguments
 * \param argv command line arguments
 * \param pmode pointer to work mode structure
 * \returns true if all necessry parameters are set
 * \returns false if program should exit
 */
bool process_command_line(const int argc, char * const argv[], struct workmode* pmode);

/// Print program usage
void usage();

/**
 * \brief Print retrieved terminal characteristics
 * \param[in] fd File descriptor of terminal (retrieved by \see open
 * \param[in] pterm Structure with terminal parameters
 */
void display_attribs(const struct termios* pterm);

/**
 * \brief Set terminal attributes
 * \param[in] fd File descriptor of terminal
 * \param[inout] pterm terminal parameters struct
 * \param[in] pconfig settings to apply to terminal/serial device
 * \param verbose true if debug printing is on
 * \returns 0 on success
 */
int set_attribs(const int fd, struct termios* pterm, const struct serial_config* pconfig);

/// Fill workmode struct with default values
void init_default_workmode(struct workmode* pmode);

void init_default_parameters();

void finish_parameter_init();

/** Restore atributes of terminal
 * \param[in] fd File descriptor of terminal
 * \param[in] pterm terminal parameters struct
 * \param verbose true if debug printing is on
 * \returns 0 on success
 * \returns error code on system call failure
 */
int restore_attribs(const int fd, const struct termios* pterm);

/** Load program limits on exes
 * \param[in] config_file name of configuration file. If NULL, "serialtest.ini"
 * is used instead
 * \param verbose true if detailed output is desired
 */
void load_settings(const char* config_file, bool verbose);

/** Save settings
 * \note ini file is regenerated. All content except for settings is discarded.
 * \param[in] config_file name of configuration file. If NULL, "serialtest.ini"
 */
void save_settings(const char* config_file, bool verbose);

/** Combine serial port settings from command line and ini-file
 * For all ports that do not have settings specified in command line,
 * use settings from initialisation file
 * \param[inout] mode port settings from command line
 */
void finish_port_init(struct workmode* mode);

#endif // _INITIALISATION_H_
