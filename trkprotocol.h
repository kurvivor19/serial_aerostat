/** \file trkprotocol.h
 * structures and messages used in trk protocol
 */

#ifndef _TRKPROTOCOL_H_
#define _TRKPROTOCOL_H_

#include "global.h"

#include <stdint.h>

enum __attribute__ ((packed)) command_codes
{
    CMD_STAT,	//<- Получить текущее состояние приводов СНА
    CMD_STOP,	//<- Остановить движение по всем осям
    CMD_GTIME,	//<- Получить текущее время на СНА
    CMD_STIME,	//<- Установить текущее время на СНА
    CMD_THEAD,	//<- Начать загрузку траектории
    CMD_TCONT,	//<- Передать траекторные данные
    CMD_MPNT,	//<- Переместить СНА в заданную точку
    CMD_MMAN,	//<- Провести движение СНА в заданном направлении
    CMD_ALOG,	//<- Запросить получение лога последнего сопровождения
    CMD_RLOG,	//<- Запросить очередную часть лога
    CMD_RPAR,	//<- Запросить значение параметра
    CMD_SPAR,	//<- Установить значение параметра
    CMD_SDIR,	//<- Установить значение ориентации антенны
    CMD_DDIR,	//<- Сбросить значение ориентации антенны
    CMD_TDEL,	//<- Change moment of the trajectory start
    CMD_COUNT	//<- Не команда!
};

typedef struct __attribute__ ((packed)) _cmd_stime_data
{
    uint8_t code;	//<- equal CMD_STIME
    uint32_t utc_time;
    int8_t tz;
} cmd_stime_data;

typedef struct __attribute__ ((packed)) _cmd_tdel_data
{
    uint8_t code;	//<- equal CMD_TDEL
    int32_t start_delta;//<- also in deciseconds
} cmd_tdel_data;

typedef struct __attribute__ ((packed)) _cmd_thead_data
{
    uint8_t code;	//<- equal CMD_THEAD
    int32_t start_time;
    uint8_t deciseconds;
    uint16_t length;
    float azimuth;
    float elevation;
} cmd_thead_data;

struct __attribute__ ((packed)) traj_point
{
    float azimuth;
    float elevation;
    int32_t time;
};

typedef struct __attribute__ ((packed)) _cmd_mpnt_data
{
    uint8_t code;	//<- equal CMD_MPNT
    float azimuth;
    float elevation;
} cmd_mpnt_data;

enum __attribute__ ((packed)) direction_codes
{
    DIR_STOP = 0,
    DIR_AZ_POS,
    DIR_AZ_NEG,
    DIR_EL_POS,
    DIR_EL_NEG
};

typedef struct __attribute__ ((packed)) _cmd_mman_data
{
    uint8_t code;	//<- equal CMD_MMAN
    uint8_t direction;
} cmd_mman_data;

enum __attribute__ ((packed)) parameter_codes
{
    PAR_AZ_SPD,
    PAR_AZ_ACC,
    PAR_AZ_DEC,
    PAR_EL_SPD,
    PAR_EL_ACC,
    PAR_EL_DEC
};

typedef struct __attribute__ ((packed)) _cmd_rpar_data
{
    uint8_t code;	//<- equal CMD_RPAR
    uint8_t parameter;
} cmd_rpar_data;

typedef struct __attribute__ ((packed)) _cmd_spar_data
{
    uint8_t code;	//<- equal CMD_RPAR
    uint8_t parameter;
    float value;
} cmd_spar_data;

typedef struct __attribute__ ((packed)) _cmd_sdir_data
{
    uint8_t code;	//<- equal CMD_RPAR
    float azimuth;
    float elevation;
} cmd_sdir_data;

enum __attribute__ ((packed)) answer_codes
{
    ANS_ERR=1,	//<- Оповещение об ошибке при рпиёме предыдущей команды
    ANS_TIME,	//<- Текущие времяф и временной пояс на СНА
    ANS_STAT,	//<- Подтверждение начала приёма траектории
    ANS_LACK,	//<- Подтверждение начала передачи логов
    ANS_LCONT,	//<- Передача фрагмента логов
    ANS_PAR,	//<- Передача значения параметра
    ANS_TTRAJ	//<- Время начала траектории
};                       

typedef struct __attribute__ ((packed)) _ans_err_data
{
    uint8_t code;	//<- equal ANS_ERR
    uint8_t err;
} ans_err_data;

enum __attribute__ ((packed)) err_codes
{
    ERR_MANGLED,
    ERR_NODATA,
    ERR_OP,
    ERR_INT,
    ERR_FAIL
};

typedef struct _cmd_stime_data ans_time_data;

typedef struct __attribute__ ((packed)) _ans_stat_data
{
    uint8_t code;	//<- equal ANS_STAT
    uint8_t end_flags;
    uint8_t system_flags;
    float azimuth;
    float az_speed;
    float elevation;
    float el_speed;
    float temp1;
    float temp2;
} ans_stat_data;

enum __attribute__ ((packed)) endpoint_flags
{
    AXIS1_LHE = 0x1,
    AXIS1_RHE = 0x2,
    AXIS2_LHE = 0x4,
    AXIS2_RHE = 0x8
};

enum __attribute__ ((packed)) heater_flags
{
    TEMP1 = 0x10,
    TEMP2 = 0x20,
    HEAT1 = 0x40,
    HEAT2 = 0x80
};

enum __attribute__ ((packed)) status_flags
{
    AXIS1_RDY = 0x1,
    AXIS1_ERR = 0x2,
    AXIS2_RDY = 0x4,
    AXIS2_ERR = 0x8,
    SEND_LOG = 0x10,
    RCV_TRAJ  = 0x20,
    WAIT_TRAJ = 0x40,
    FLW_TRAJ = 0x80
};

typedef struct __attribute__ ((packed)) _ans_lack_data
{
    uint8_t code;	//<- equal ANS_TACK
    uint16_t length;
} ans_lack_data;


typedef struct __attribute__ ((packed)) _ans_par_data
{
    uint8_t code;	//<- equal ANS_PAR
    uint8_t parameter;
    float value;
} ans_par_data;

typedef struct __attribute__ ((packed)) _ans_ttraj_data
{
    uint8_t code;	//<- equal ANS_TTRAJ
    int32_t utc_time;
    uint8_t deciseconds;
} ans_ttraj_data;

#endif // _TRKPROTOCOL_H_
