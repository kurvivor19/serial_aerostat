/** \file global.h
 * enums and global variables
 */

#ifndef _GLOBAL_H_
#define _GLOBAL_H_

#include <stdbool.h>
#include <stdint.h>
#include <time.h>
#include <sys/time.h>

enum operation_modes
{
    mIdle,		///< No axes are moving
    mMovePnt,		///< Movement to the set point
    mMoveMan,		///< Movement to the given direction
    mWaitStartTime,	///< Waiting for start of trajectory
    mTracking,		///< Following the trajectory
    mNotReady,		///< There is no readiness signal from one or more servo-ampliphyers
    mAlarm		///< There is an alarm signal from one or more servo-amplifyers
};

#ifndef FREQUENCY
#define FREQUENCY 100
#endif

#ifndef AXIS_SPEED_BUFFER_SIZE
#define AXIS_SPEED_BUFFER_SIZE 25
#endif

struct axis_state
{
    bool endpoint[2];	///< signals from endpoints
    bool limit[2];	///< checks for going from \see program_limits_pos and \see program_limits_neg
    bool inp;		///< flag of being in correct position
    bool alarm, ready;	///< error and readiness singlas from servo-amplifier
    int32_t current_position; ///< position of the sensor's axis
    int32_t prev_position; ///< previous position of the axis (used for gradual acceleration)
    /// buffer of the previous positions of the axis
    int32_t old_positions[AXIS_SPEED_BUFFER_SIZE];
    /// buffer of the times when previous positions were recorded
    suseconds_t prev_times_msec[AXIS_SPEED_BUFFER_SIZE];
    int speed_cursor;	///< cursor for the last position/last time buffers; used for speed calculations
    float real_speed;	///< speed calculated from position buffer, degrees/sec
    bool start;		///< flag for the beginning of the axis movement to the point
    bool stopped;	///< flag for stopping the movement of the axis
    bool accelerate;	///< flag for acceleration of the axis movement to the point
    int32_t target_position; ///< position the axis is moving towards
    int32_t max_speed;	///< speed limit for the movement (in discreets)
    int32_t min_speed;	///< minimal movement speed (in discreets); movement speed less then it will be discarded
    int32_t zprivod;	///< frequency sent to servo-amplifier
};

/// state of the heating system
struct termo_state
{
    int heating[2];	///< indicators of the heaters working
    int sensor[2];	///< indication if the thermo sensors working
    int temperature[2];	///< termparature values (celcius)
};

/// full state for the drive system
struct drive_states
{
    uint8_t flag;	///< status from the drives as a bitfield
    /// current mode of operations
    enum operation_modes operation_mode;
    time_t time_sec;	///< current time
    /// atates of axes
    struct axis_state axis[2];
};

struct axis_limits
{
    float limit_pos;
    float limit_neg;
    float speed_max;
    float speed_min;
};

struct axis_man_limits
{
    float speed_max;
    float accel;
    float decel;
};

struct all_parameters
{
    float K1[2];		///< Proportional koefficient for position error
    float KS[2];		///< Koefficient for negative feedback for movement
    float KK[2];		///< Koefficient for speed error compensation
    float KI[2];		///< Integral  coefficient
    /// limits for axismovement
    struct axis_limits axis_limits[2];
    uint32_t DPR;		///< Discreets per rotation
    uint32_t SPDMAX;		///< Maximum speed (discreets)
    uint32_t SPDMIN;		///< Minimum speed (discreets)
    double SPDMAXDEG;
    /// Coefficient that defines proportion between speed in discreets (per second) and frequency
    float KZ2D;
    struct axis_man_limits axis_man_limits[2];
    float azimuth_delta;
    float elevation_delta;

    int SensorSigns[2];
    int AmplifierDirectionFlags[2];
};

extern struct all_parameters default_parameters, current_parameters;

#endif // _GLOBAL_H_
