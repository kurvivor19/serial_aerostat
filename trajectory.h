/** \file trajectory.h
 * Trajectory data buffer and memory structures
 */

#ifndef _TRAJECTORY_H_
#define _TRAJECTORY_H_

#include "global.h"

#include <stdint.h>
#include <time.h>

#define MAX_MEMORY_SIZE 10000

#define MSEC_INTERVAL 100

struct trajectory_point
{
   float axis_pos[2];
   int time_msec;
};

struct trajectory_header
{
    time_t start_time;
    int deciseconds;
    int start_delta;	//<- also in deciseconds
    int length;
    void* pimpl;
};

enum data_modes
{
    dmIdle,
    dmRecieving,
    dmSending
};

struct trajectory_state
{
    struct trajectory_header trajectory;
    struct trajectory_header log;
    int trajectory_cursor, log_cursor;
    enum data_modes data_mode;
};

/** initialise trajectory buffer
 * Is a constructor
 * \patam[inout] pheader trajectory header structure
 * must have all fields except for pimpl properly initialised
 * \returns 0 on success
 */
int init_trajectory(struct trajectory_header* pheader);

/** clean trajectory buffer
 * Is a descturctor
 * \param[inout] pheader initialised trajectory header structure
 * \returns 0 on success
 */
int clean_trajectory_header(struct trajectory_header* pheader);

/** Get trajectory point for writing
 * \param[in] pheader header of trajectory structure
  * \param point_number number of trajectory point
 * this is a number of point IN TRAJECTORY, not in buffer
 * buffer is wrapping around itself, so there is no guarantee
 * point will be avaiable
 * \returns NULL for points not in part of trajectory currently covered by buffer
 * \returns pointer to point of trajectory
 */
struct trajectory_point* get_write_point(const struct trajectory_header* pheader, int point_number);

/** Get trajectory point for reading
 * \param[in] pheader header of trajectory structure
 * \param point_number number of trajectory point
 * this is a number of point IN TRAJECTORY, not in buffer
 * buffer is wrapping around itself, so there is no guarantee
 * point will be avaiable
 * \returns NULL for points not in part of trajectory currently covered by buffer
 * \returns pointer to point of trajectory
 */
const struct trajectory_point* get_read_point(const struct trajectory_header* pheader, int point_number);

struct trajectory_point* get_next_point(const struct trajectory_header* pheader);

void reinit_trajectory(struct trajectory_header* pheader);

/** Get number of currently loaded points
 * \param[in] pheader trajectory object
 * \returns number of already loaded points
 */
int get_current_size(const struct trajectory_header* pheader);

#endif // _TRAJECTORY_H_
