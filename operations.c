/** \file operations.c
 * Contains main loop of the program
 * Performs either listening on the serial port or broadcasting on it
 */

#include "trajectory.h"
#include "bupprotocol.h"
#include "trkprotocol.h"
#include "utils.h"
#include "display.h"
#include "errors.h"
#include "operations.h"

#include <stddef.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <sched.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <netinet/tcp.h>
#include <fcntl.h>
#include <math.h>
#include <stdlib.h>

struct shared_state shs;
/// Set to the signal that is reason for termination
volatile sig_atomic_t reason = 0;

/** check if all drives are ready
 * \returns true at least drive is not ready
 */
bool not_ready(const struct drive_states* pstate);

/** Holds program in current state
 * Program awaits until condition is fulfilled or program exit is requested
 * \param ptty_control file descriptor of active device (BUP)
 * \param pstate current state of the system
 * \param tester function that evaluates if condition is met
 * \note function exits when tester returns false
 * \returns true if condition of state change is met upon function's completion
 */
bool drive_state_short_circuit(int ptty_active, struct drive_states* pstate,
                               bool(*tester)(const struct drive_states* pstate));

/** Recieve and process data on a port
 * Recieves and uses context for current recieving state
 * \param fid file descriptor for recieving data
 * \note if bad error occurs, is closed and reset to -1
 * \param offset place in raw buffer where data can be written
 * \param buffer preallocated buffer for incoming data
 * \note contains useful data upon successful completion
 * \returns true if there are now data to be processed
 */
int recieve_tcp_data(int* fid, int* offset,
                     unsigned char* buffer);

/** Accept or discard newly incoming connection over tcp
 * \param[in] socket socket on which connection is incoming
 * \param has_connection flag that shows if there is already an established connection
 * \returns new file descriptor or -1 if connection is rejected
 */
int handle_incoming_connection(int socket, bool has_connection);

/** Asynchronous saving of settings
 * \param[in] arg name of file to save data into
 */
void* settings_threadfunc(void* arg);

int operate(int ptty_active, struct tcp_config* pport_control, int ptty_echo, int ptty_data,
            int ptty_termo, struct workmode* pmode)
{
   // check parameter sanity
   if(pmode == NULL || pport_control == NULL)
      return invalid_parameter;

   // prepare shared state
   memset(&shs, 0, sizeof(shs));
   shs.limitslock = (pthread_mutex_t)PTHREAD_MUTEX_INITIALIZER;
   shs.commandlock = (pthread_mutex_t)PTHREAD_MUTEX_INITIALIZER;
   shs.trajectorylock = (pthread_mutex_t)PTHREAD_MUTEX_INITIALIZER;
   shs.statelock = (pthread_mutex_t)PTHREAD_MUTEX_INITIALIZER;
   shs.statelock = (pthread_mutex_t)PTHREAD_MUTEX_INITIALIZER;
   shs.settingslock = (pthread_mutex_t)PTHREAD_MUTEX_INITIALIZER;

   // flag describing if we should try and save settings now
   bool request_saving_settings = false;
   
   // setup buffers and flags for inter-thread communications
   LOGPRINT("Creating buffers\n");
   if(!!init_trajectory(&shs.current_traj_data.trajectory))
   {
      refresh_display();
      return general_failure;
   }
   if(!!init_trajectory(&shs.current_traj_data.log))
   {
      refresh_display();
      return general_failure;
   }
   shs.current_traj_data.data_mode = dmIdle;
   memset((void*)shs.current_state, 0, 2 * sizeof(*shs.current_state));
   shs.newstate = 0;

   // setup buffer for incoming commands
   unsigned char buffer_raw[260], outbuffer_raw[260];
   memset(buffer_raw, 0, 260);
   memset(outbuffer_raw, 0, 260);
   /// offset - point in 'dirty' buffer where new data should be written
   /// rebase - beginning of next packet
   size_t offset = 0;


   int res = thread_setup(!pmode->mute);
   if(res) return res;

   // initialise set if interrupting signals
   sigset_t interrupt, blockio, restore;
   //sigemptyset(&interrupt);
   sigfillset(&interrupt);
   sigdelset(&interrupt, SIGTERM);
   sigdelset(&interrupt, SIGINT);

   sigemptyset(&blockio);
   sigaddset(&blockio, SIGIO);
   sigaddset(&blockio, SIGALRM);

   
   //ualarm(interval, interval);
   struct itimerval timer_interval = {{1, 0}, {1, 0}};

   LOGPRINT("Spawning threads\n");
   struct work_thread threaddata = {0, ptty_active, ptty_data, 0};

   if(!!pthread_create(&threaddata.wpid, NULL, threadfunc, &threaddata))
   {
      LOGPRINT("Failed to set start thread.\nError #%d(%s)\n",
               errno, strerror(errno));
      refresh_display();
      return general_failure;
   }

   LOGPRINT("Sensors thread spawned");

   struct work_thread_termo threaddata2 = {0, ptty_termo};
   if(ptty_termo > 0)
   {
       if(!!pthread_create(&threaddata2.wpid, NULL, threadfunc_termo, &threaddata2))
           LOGPRINT("Failed to set start thread.\nError #%d(%s)\n",
                    errno, strerror(errno));
       else
           LOGPRINT("Termo thread spawned");
       refresh_display();
   }

   fd_set base_set, read_set;

   if(!!pthread_sigmask(SIG_BLOCK, &blockio, &restore))
   {
      printf("Failed to set sigmask.\nError #%d(%s)\n",
             errno, strerror(errno));
      refresh_display();
      return general_failure;
   }

   LOGPRINT("Setting signal masks\n");
   FD_ZERO(&base_set);
   FD_SET(pport_control->socket, &base_set);

   res = 0;
   while(!shs.done)
   {
      request_saving_settings = false;
      FD_ZERO(&read_set);
      // only signal interrupts are interesting
      // we wait if there are no data or last packet is not finished
      if(offset == 0)
      {
         read_set = base_set;
         if(pport_control->file != -1)
         {
            FD_SET(pport_control->file, &read_set);
         }
         struct timespec tv = {0, 1000 * 1000 * 200};

         res = pselect(FD_SETSIZE, &read_set, NULL, NULL, &tv, &interrupt);
      }
      else
      {
         res = 1;
      }
      // TODO: raise refresh rates
      update_outer_stat();
      display_state((struct drive_states*)&shs.current_state[1],
                    &shs.current_traj_data, shs.current_traj_data.data_mode,
                    (ptty_termo > 0? &shs.current_temperature: NULL));
      refresh_display();
      if(res == 0)
      {
          // timeout occured
          if(pport_control->file == -1)
              print_message(CONTROL_MESSAGE, "Timeout on control port\n");
      }
      else
      {
         if(res < 0)
         {
            if(errno == EINTR)
            {
               // interrupt occured
               print_message(CONTROL_MESSAGE, "Wait for activity on control port interrupted\n");
            }
         }
         else
         {
            // check if there is incoming connection
            if(FD_ISSET(pport_control->socket, &read_set))
            {
               res = handle_incoming_connection(pport_control->socket,
                                                pport_control->file != -1);
               if(res != -1)
                  pport_control->file = res;
            }
            // if we presume to still have some unprocessed data, there is no need
            // to check for set file descriptor in the set
            if(FD_ISSET(pport_control->file, &read_set)/* || offset > 0 */)
            {
               res = recieve_tcp_data(&pport_control->file, &offset,
                                      buffer_raw);
               if(res == VALID)
               {
                  request_saving_settings = process_incoming_command((struct bup_data_block_header*)buffer_raw,
                                                                     (struct bup_data_block_header*)outbuffer_raw,
                                                                     260, &threaddata);
                  // answer
                  write_bup_data((struct bup_data_block_header*)outbuffer_raw, pport_control->file);
                  // clear buffer
                  memset(buffer_raw, 0, 260);;
                  offset = 0;
               }
               else
               {
                  if(res == MISALIGNED ||
                     res == CORRUPT)
                  {
                     struct bup_data_block_header* pans_header = (struct bup_data_block_header*)outbuffer_raw;
                     ans_err_data* pans_data = (ans_err_data*)(pans_header + 1);
                     pans_data->code = ANS_ERR;
                     pans_data->err = ERR_MANGLED;
                     pans_header->size = sizeof(ans_err_data);
                     write_bup_data(pans_header, pport_control->file);
                     // clear buffer
                     memset(buffer_raw, 0, 260);;
                     offset = 0;
                  }
                  // for bad errors, we clear buffer
                  // bad errors set fd to -1
                  if (res < 0 && pport_control->file < 0)
                  {
                      // clear buffer
                      memset(buffer_raw, 0, 260);;
                      offset = 0;
                  }
               }
            } // newdata read 

         } // new input avaiable
      } // if timeout
      if(request_saving_settings)
      {
          // try to claim settingslock
          if(pthread_mutex_trylock(&shs.settingslock))
          {
          // claim the settinglock and start saving
              request_saving_settings = false;
              print_message(CONTROL_MESSAGE, "Saving settings...\n");
              pthread_mutex_unlock(&shs.settingslock);
              pthread_t settings_thread;
              /* pthread_create(&settings_thread, NULL, settings_threadfunc, pmode->ini_file); */
              /* pthread_detach(settings_thread); */
              settings_threadfunc(pmode->ini_file);
          }
      }
   }
    
   LOGPRINT("Stopped loop on signal %d.\nJoining threads\n", reason);
   void* threadres, *threadres2;
   // sending alarm signal to wake up work thread
   pthread_kill(threaddata.wpid, SIGALRM);
   if(ptty_termo > 0)
   {        
       pthread_kill(threaddata2.wpid, SIGALRM);
       pthread_join(threaddata2.wpid, NULL);
   }

   // alternatively
   // kill(0, SIGALARM);
   pthread_join(threaddata.wpid, &threadres);
   LOGPRINT("Threads joined\n");
   pthread_mutex_destroy(&shs.statelock);
   pthread_mutex_destroy(&shs.commandlock);

   // wait for mutex to be freed by any settings saving thread
   // that thread is detached, so no join is needed
   /* pthread_mutex_lock(&shs.settingslock); */
   /* pthread_mutex_unlock(&shs.settingslock); */
   pthread_mutex_destroy(&shs.settingslock);
    
   clean_trajectory_header(&shs.current_traj_data.trajectory);
   clean_trajectory_header(&shs.current_traj_data.log);
   pthread_sigmask(SIG_SETMASK, &restore, NULL);
   LOGPRINT("%d bytes read in total.\n", threaddata.total);
   // int ptty_control, int ptty_echo, int ptty_data

   tcflush(ptty_data, TCIOFLUSH);
   if(ptty_echo > 0)
      tcflush(ptty_echo, TCIOFLUSH);

   return success;
}

void* settings_threadfunc(void* arg)
{
    char* ini_file = (char*)arg;
    pthread_mutex_lock(&shs.settingslock);
    save_settings(ini_file, false);
    pthread_mutex_unlock(&shs.settingslock);
    return NULL;
}

// #define print_message if (0) print_message

void finish(int signum)
{
    shs.done = true;
    reason = signum;
}

void raisealarm(int sugnum)
{
    // reraise alarm
    alarm(1);
}

void indata(int sugnum)
{
    shs.input = 1;
}

void update_outer_stat()
{
    // in any case, update state information
    if(shs.newstate)
    {
        pthread_mutex_lock(&shs.statelock);
        memcpy((void*)&shs.current_state[1], (void*)&shs.current_state[0], sizeof(*shs.current_state));
        shs.newstate = 0;
        pthread_mutex_unlock(&shs.statelock);
    }
}

uint32_t get_time()
{
    // get time in msec from start of day
    struct timeval now;
    gettimeofday(&now, NULL);
    // TODO: take day change into account
    if(now.tv_sec > day_end)
    {
        day_start = day_end;
        day_end = day_start - day_len;
    }
    return (now.tv_sec - day_start) * 1000 + now.tv_usec / 1000;
}

int set_time(uint32_t newtime)
{
    struct timeval tv;
    tv.tv_sec = day_start + newtime / 1000;
    tv.tv_usec = (newtime % 1000) * 1000;
    
    return settimeofday(&tv, NULL);
}

int thread_setup()
{
    // raise the level of thread privileges
    LOGPRINT("Locking pages\n");
    
    if(!!mlockall(MCL_CURRENT))
    {
        printf("Failed to lock memory.\nError #%d(%s)\n",
               errno, strerror(errno));
        refresh_display();
        return privileges;
    }
    refresh_display();
    LOGPRINT("Sheduling to run as Round Robin process\n");
    struct sched_param parameter;
    // priority value holds no meaning here, except for it being larger than zero
    parameter.sched_priority = 19;
    if(!!sched_setscheduler(0, SCHED_RR, &parameter))
    {
        printf("Failed to set up sheduler.\nError #%d(%s)\n",
               errno, strerror(errno));
        refresh_display();
        return privileges;
    }
    
    refresh_display();
    // setup signal handlers
    struct sigaction actions[] = {
        {finish, 0, 0, 0, 0},
        {raisealarm, 0, 0, 0, 0},
        {indata, 0, 0, 0, 0}
    };
    if(!!sigaction(SIGTERM, actions, NULL))
    {
        printf("Failed to set SIGTERM handler.\nError #%d(%s)\n",
               errno, strerror(errno));
        refresh_display();
        return general_failure;
    }
    if(!!sigaction(SIGINT, actions, NULL))
    {
        printf("Failed to set SIGINT handler.\nError #%d(%s)\n",
               errno, strerror(errno));
        refresh_display();
        return general_failure;
    }
    if(!!sigaction(SIGALRM, actions + 1, NULL))
    {
        printf("Failed to set SIGALRM handler.\nError #%d(%s)\n",
               errno, strerror(errno));
        refresh_display();
        return general_failure;
    }
    if(!!sigaction(SIGIO, actions + 2, NULL))
    {
        printf("Failed to set SIGIO handler.\nError #%d(%s)\n",
               errno, strerror(errno));
        refresh_display();
        return general_failure;
    }
    
    refresh_display();
    return success;
}

bool any_alarm(const struct drive_states* pstate)
{
    if(!pstate)
        return true;
    for(int i = 0; i < 2; ++i)
        if(pstate->axis[i].alarm)
            return true;
    return false;
}

int handle_incoming_connection(int socket, bool has_connection)
{
    struct sockaddr adress;
    socklen_t socklen = sizeof(adress);
    int cfd = accept(socket, &adress, &socklen);
    if(cfd != -1)
    {
        if(has_connection)
        {
            print_message(CONTROL_MESSAGE, "Discarding incoming connection\n");
            close(cfd);
            return -1;
        }
        else
        {
            char hostname[50];
            getnameinfo(&adress, socklen,
                        hostname, 50, NULL, 0,
                        NI_NUMERICHOST);
            int optval = 1;
            print_message(CONTROL_MESSAGE, "Accepted connection from %s", hostname);
            if((setsockopt(cfd, SOL_SOCKET, SO_KEEPALIVE,
                           (void *)&optval, sizeof(optval)) == -1) ||
               (fcntl(cfd, F_SETFL, O_NONBLOCK) == -1) ||
               (setsockopt(cfd, IPPROTO_TCP, TCP_NODELAY,
                           (char *) &optval, sizeof(optval)) == -1))
            {
                close(cfd);
                print_message(CONTROL_MESSAGE, "Failed to set file descriptor options\n");
                return -1;
            }
            else
            {
                return cfd;
            }
        }
    }
    return -1;
}

int recieve_tcp_data(int* fid, int* offset,
                     unsigned char* buffer)
{
    // sanity check
    static int failures_count = 0;
    const int failures_limit = 5;
    const int buffer_size = 260;
    if(fid == NULL || offset == NULL ||
       buffer == NULL)
    {
        return false;
    }

    int res = 0, cursor = *offset, valres = PENDING_HEADER;
    bool stop = false;
    do
    {
        res = read(*fid, buffer + cursor, 1);
        if(res < 1)
            continue;
        cursor += res;
        valres = validate_bup_data(buffer, cursor);
        switch(valres)
        {
        case MISALIGNED:
            *offset = 0;
            print_message(CONTROL_MESSAGE, "Recieved misaligned byte %s\n", make_hex_string(buffer, 1));
            cursor = 0;
            continue;
        case PENDING_HEADER:
        case PENDING_DATA:
            if(cursor >= buffer_size)
                stop = true;
            break;
        case VALID:
        case CORRUPT:
            stop = true;
            break;
        }
    }while((res > 0) && !stop);
    int diff = cursor - *offset;
    *offset = cursor;
    if(diff > 0)
    {
        print_message(CONTROL_MESSAGE, "Recieved packet of %d bytes in control port\n", diff);
        print_message(CONTROL_MESSAGE, "Data recieved: %s\n", make_hex_string(buffer + *offset - diff, diff));
    }
    // handle error if any occured during read
    if(res < 0)
    {
        // only 'good' errors is EINTR, EAGAIN and EWOULDBLOCK in that case we can just read again
        // other errors cause connection to be closed
        if(!(errno == EAGAIN || errno == EWOULDBLOCK ||
             errno == EINTR))
        {
            print_message(CONTROL_MESSAGE, "Error #%d during read (%s)\n", errno, strerror(errno));
            print_message(CONTROL_MESSAGE, "Bad error, closing connection\n");
            close(*fid);
            *fid = -1;
            return -1;
        }
    }
    if(res == 0)
    {
        print_message(CONTROL_MESSAGE, "Connection closed\n");
        close(*fid);
        *fid = -1;
        return valres;
    }
    
    if(valres != VALID)
    {
        LOGPRINT("Packet is empty or malformed\n");
        return valres;
    }
    else
        failures_count = 0;
    return valres;
}

int make_ans_time(void* pdata)
{
    struct timeval tv;
    struct timezone tz;
    if(0 == gettimeofday(&tv, &tz))
    {
        ans_time_data *pans = (ans_time_data*)pdata;
        pans->code = ANS_TIME;
        pans->utc_time = tv.tv_sec;
        pans->tz = tz.tz_minuteswest / 60;
        return sizeof(ans_time_data);
    }
    else
    {
        ans_err_data *pans = (ans_err_data*)pdata;
        pans->code = ANS_ERR;
        pans->err = ERR_OP;
        return sizeof(ans_err_data);
    }
    
}

int try_set_time(cmd_stime_data* cmd, void* pdata, enum operation_modes current_mode)
{
    if(current_mode == mTracking)
    {
        ans_err_data *pans = (ans_err_data*)pdata;
        pans->code = ANS_ERR;
        pans->err = ERR_FAIL;
        print_message(CONTROL_MESSAGE, "Refused to set time: tracking in progress.\n");
        return sizeof(ans_err_data);
    }
    struct timeval tv = { cmd->utc_time, 0 };
    struct timezone tz = { 60 * cmd->tz, 0 };
    if(0 == settimeofday(&tv, &tz))
    {
        // reinitialise day_start and day_end
        time_t now = time(NULL);
        struct tm* today = gmtime(&now);
        today->tm_sec = 0;
        today->tm_min = 0;
        today->tm_hour = 0;
        day_start = mktime(today);
        day_end = day_start + day_len;

        return make_ans_time(pdata);
    }
    else
    {
        ans_err_data *pans = (ans_err_data*)pdata;
        pans->code = ANS_ERR;
        pans->err = ERR_OP;
        print_message(CONTROL_MESSAGE, "Failed to set time.\nError #%d(%s)\n",
                      errno, strerror(errno));
        return sizeof(ans_err_data);
    }
}

int move_to_position(cmd_mpnt_data* cmd, uint8_t* pans_code, enum operation_modes current_mode)
{
    if(current_mode >= mNotReady)
    {
        ans_err_data *pans = (ans_err_data*)pans_code;
        print_message(CONTROL_MESSAGE, "Refused to move: drives cannot comply.\n");
        pans->code = ANS_ERR;
        pans->err = ERR_FAIL;
        return sizeof(ans_err_data);
    }
    pthread_mutex_lock(&shs.commandlock);
    shs.bup_command = CMD_MPNT;
    
    shs.target_position.position[0] = cmd->azimuth - current_parameters.azimuth_delta;
    shs.target_position.position[1] = cmd->elevation - current_parameters.elevation_delta;
    // always constant now!
    for(int i = 0; i < 2; ++i)
    {
        shs.target_speed.speed[i] = current_parameters.axis_limits[i].speed_max / 2;
    }
    // target_speed.speed[axis_ind] = input->AxisMoveTo.Spd;
    pthread_mutex_unlock(&shs.commandlock);
    if(current_mode > mMoveMan)
    {
        ans_err_data *pans = (ans_err_data*)pans_code;
        print_message(CONTROL_MESSAGE, "Interrupting trajectory following.\n");
        pans->code = ANS_ERR;
        pans->err = ERR_INT;
        return sizeof(ans_err_data);
    }
    else
    {
        return 0;
    }
}

int move_to_direction(cmd_mman_data* cmd, uint8_t* pans_code, enum operation_modes current_mode)
{
    if(current_mode >= mNotReady)
    {
        ans_err_data *pans = (ans_err_data*)pans_code;
        print_message(CONTROL_MESSAGE, "Refused to move: drives cannot comply.\n");
        pans->code = ANS_ERR;
        pans->err = ERR_FAIL;
        return sizeof(ans_err_data);
    }
    if(cmd->direction > DIR_EL_NEG)
    {
        ans_err_data *pans = (ans_err_data*)pans_code;
        print_message(CONTROL_MESSAGE, "Unknown direction code.\n");
        pans->code = ANS_ERR;
        pans->err = ERR_FAIL;
        return sizeof(ans_err_data);
        
    }
    pthread_mutex_lock(&shs.commandlock);
    shs.bup_command = CMD_MMAN;

    shs.target_direction = cmd->direction;

    // target_speed.speed[axis_ind] = input->AxisMoveTo.Spd;
    pthread_mutex_unlock(&shs.commandlock);
    if(current_mode > mMoveMan)
    {
        ans_err_data *pans = (ans_err_data*)pans_code;
        print_message(CONTROL_MESSAGE, "Interrupting trajectory following.\n");
        pans->code = ANS_ERR;
        pans->err = ERR_INT;
        return sizeof(ans_err_data);
    }
    else
    {
        return 0;
    }    
}

int accept_new_trajectory(cmd_thead_data* cmd, void* pdata, enum operation_modes current_mode)
{
    if(current_mode == mNotReady ||
       current_mode == mAlarm)
    {
        ans_err_data *pans = (ans_err_data*)pdata;
        print_message(CONTROL_MESSAGE, "Refused to accept trajectory: drives cannot comply.\n");
        pans->code = ANS_ERR;
        pans->err = ERR_FAIL;
        return sizeof(ans_err_data);
    }
    bool interrupt = false;
    // trajectory following is interrupted
    if(current_mode >= mMovePnt)
    {
        interrupt = true;
    }
    if(shs.current_traj_data.data_mode != dmIdle)
        interrupt = true;
    shs.current_traj_data.trajectory.length = cmd->length;
    shs.current_traj_data.trajectory.start_time = (time_t)cmd->start_time;
    shs.current_traj_data.trajectory.deciseconds = cmd->deciseconds;
    // done in reinit... call
    //shs.current_traj_data.trajectory.start_delta = 0;
    shs.current_traj_data.log.length = cmd->length;
    shs.current_traj_data.log.start_time = (time_t)cmd->start_time;
    shs.current_traj_data.log.deciseconds = cmd->deciseconds;

    reinit_trajectory(&shs.current_traj_data.trajectory);
    reinit_trajectory(&shs.current_traj_data.log);

    struct trajectory_point* ppoint = get_write_point(&shs.current_traj_data.trajectory, 0);
    ppoint->axis_pos[0] = cmd->azimuth -
        current_parameters.azimuth_delta;
    ppoint->axis_pos[1] = cmd->elevation -
        current_parameters.elevation_delta;
    // time_msec is not set here
    // TODO: add proper command?..
    pthread_mutex_lock(&shs.commandlock);
    shs.bup_command = CMD_THEAD;
    shs.current_traj_data.data_mode = dmRecieving;
    pthread_mutex_unlock(&shs.commandlock);
    
    if(!interrupt)
    {
	ans_ttraj_data* pans = (ans_ttraj_data*)pdata;
	pans->code = ANS_TTRAJ;
	pans->utc_time = shs.current_traj_data.trajectory.start_time;
        pans->deciseconds = shs.current_traj_data.trajectory.deciseconds;
	return sizeof(ans_ttraj_data);
    }
    else
    {
        ans_err_data *pans = (ans_err_data*)pdata;
        pans->code = ANS_ERR;
        pans->err = ERR_INT;
        return sizeof(ans_err_data);
    }
}
int set_trajectory_delay(cmd_tdel_data* cmd, void* pdata, enum operation_modes current_mode)
{
    unsigned long int temp_deci;
    if(current_mode != mWaitStartTime)
    {
        ans_err_data *pans = (ans_err_data*)pdata;
        print_message(CONTROL_MESSAGE, "Not waiting for trajectory: cannot change start time.\n");
        pans->code = ANS_ERR;
        pans->err = ERR_FAIL;
        return sizeof(ans_err_data);
    }
    // check value?..
    shs.current_traj_data.trajectory.start_delta = cmd->start_delta;

    ans_ttraj_data* pans = (ans_ttraj_data*)pdata;
    pans->code = ANS_TTRAJ;
    temp_deci = 10 * (shs.current_traj_data.trajectory.start_time - day_start) +
        shs.current_traj_data.trajectory.deciseconds +
        shs.current_traj_data.trajectory.start_delta;
    pans->utc_time = temp_deci / 10;
    pans->deciseconds = temp_deci % 10;
    return sizeof(ans_ttraj_data);
}

int recieve_points(int data_size, uint8_t* in_data, void* out_data)
{
    if(shs.current_traj_data.data_mode != dmRecieving)
    {
        ans_err_data *pans = (ans_err_data*)out_data;
        print_message(CONTROL_MESSAGE, "Is not currently recieving data.\n");
        pans->code = ANS_ERR;
        pans->err = ERR_FAIL;
        return sizeof(ans_err_data);
    }
    struct traj_point *ptpoint = (struct traj_point*)(in_data + 1);
    int points_number = (data_size - 1) / sizeof(struct traj_point);
    int idx = 0;
    struct trajectory_point* pwp;
    print_message(CONTROL_MESSAGE, "Recieved %d points.\n", points_number);
    for(;pwp != NULL && (idx < points_number);
        ++idx)
    {
        pwp = get_next_point(&shs.current_traj_data.trajectory);
        if(pwp == NULL)
            break;
        pwp->axis_pos[0] = ptpoint[idx].azimuth -
            current_parameters.azimuth_delta;
        pwp->axis_pos[1] = ptpoint[idx].elevation -
            current_parameters.elevation_delta;
        pwp->time_msec = ptpoint[idx].time;
    }
    if(pwp == NULL
       // get_current_size actually returns index of the last element
       || get_current_size(&shs.current_traj_data.trajectory) >= shs.current_traj_data.trajectory.length - 1)
    {
        print_message(CONTROL_MESSAGE, "Stopped recieving trajectory.\n");
        shs.current_traj_data.data_mode = dmIdle;
        if(idx > 0)
        {
            return 0;
        }
        else
        {
            ans_err_data *pans = (ans_err_data*)out_data;
            print_message(CONTROL_MESSAGE, "Trajectory fully recieved already.\n");
            pans->code = ANS_ERR;
            pans->err = ERR_FAIL;
            return sizeof(ans_err_data);
        }
    }
    else
    {
        return 0;
    }
}

int start_send_log(void* out_data)
{
    if(shs.current_traj_data.data_mode == dmRecieving)
    {
        ans_err_data *pans = (ans_err_data*)out_data;
        print_message(CONTROL_MESSAGE, "Is currently sending data.\n");
        pans->code = ANS_ERR;
        pans->err = ERR_FAIL;
        return sizeof(ans_err_data);
    }
    int ans_size = get_current_size(&shs.current_traj_data.log);
    if(0 == ans_size)
    {
        ans_err_data *pans = (ans_err_data*)out_data;
        print_message(CONTROL_MESSAGE, "No data to send.\n");
        pans->code = ANS_ERR;
        pans->err = ERR_NODATA;
        return sizeof(ans_err_data);
    }
    shs.current_traj_data.data_mode = dmSending;
    shs.current_traj_data.log_cursor = 0;
    ans_lack_data* pans = (ans_lack_data*)out_data;
    pans->code = ANS_LACK;
    // that was actually 
    pans->length = ans_size + 1;
    return sizeof(ans_lack_data);    
}

int send_more_log(size_t avaiable_size, void* out_data)
{
    if(shs.current_traj_data.data_mode != dmSending)
    {
        ans_err_data *pans = (ans_err_data*)out_data;
        print_message(CONTROL_MESSAGE, "Is not currently sending data.\n");
        pans->code = ANS_ERR;
        pans->err = ERR_FAIL;
        return sizeof(ans_err_data);
    }
    int used_data = 0;
    uint8_t* pans_code = (uint8_t*)out_data;
    *pans_code = ANS_LCONT;
    ++used_data;
    const struct trajectory_point* pwp;
    struct traj_point *ptpoint = (struct traj_point*)(pans_code + 1);
    for(int idx = 0; pwp != NULL && ((used_data + sizeof(struct traj_point)) < avaiable_size);
        ++idx)
    {
        pwp = get_read_point(&shs.current_traj_data.log, shs.current_traj_data.log_cursor++);
        if(pwp == NULL)
            break;
        ptpoint[idx].azimuth = pwp->axis_pos[0];
        ptpoint[idx].elevation = pwp->axis_pos[1];
        ptpoint[idx].time = pwp->time_msec;
        used_data += sizeof(struct traj_point);
    }
    if(shs.current_traj_data.log_cursor > get_current_size(&shs.current_traj_data.log))
    {
        print_message(CONTROL_MESSAGE, "Finished sending log: %d points sent\n",
            get_current_size(&shs.current_traj_data.log) + 1);
        shs.current_traj_data.data_mode = dmIdle;
    }
    return used_data;
}

int send_param_value(cmd_rpar_data* in_data, void* out_data)
{
    ans_par_data* pap = out_data;
    switch(in_data->parameter)
    {
    case PAR_AZ_SPD:
        pap->value = current_parameters.axis_man_limits[0].speed_max;
        break;
    case PAR_AZ_ACC:
        pap->value = current_parameters.axis_man_limits[0].accel;
        break;
    case PAR_AZ_DEC:
        pap->value = current_parameters.axis_man_limits[0].decel;
        break;
    case PAR_EL_SPD:
        pap->value = current_parameters.axis_man_limits[1].speed_max;
        break;
    case PAR_EL_ACC:
        pap->value = current_parameters.axis_man_limits[1].accel;
        break;
    case PAR_EL_DEC:
        pap->value = current_parameters.axis_man_limits[1].decel;
        break;
    default:
        print_message(CONTROL_MESSAGE, "Unknown parameter code %d.\n",
                      in_data->parameter);
        ans_err_data* pans = out_data;
        pans->code = ANS_ERR;
        pans->err = ERR_NODATA;
        return sizeof(ans_err_data);
    }
    pap->parameter = in_data->parameter;
    pap->code = ANS_PAR;
    return sizeof(ans_par_data);
}

int set_param_value(cmd_spar_data* in_data, void* out_data)
{
    ans_par_data* pap = out_data;
    float* pparam;
    float param_lim_min = 0.f, param_lim_max;
    switch(in_data->parameter)
    {
    case PAR_AZ_SPD:
        pparam = &current_parameters.axis_man_limits[0].speed_max;
        param_lim_min = 0.5f;
        param_lim_max = 5.f;
        break;
    case PAR_AZ_ACC:
        pparam = &current_parameters.axis_man_limits[0].accel;
        param_lim_min = 0.5f;
        param_lim_max = 5.f;
        break;
    case PAR_AZ_DEC:
        pparam = &current_parameters.axis_man_limits[0].decel;
        param_lim_min = 0.5f;
        param_lim_max = 5.f;
        break;
    case PAR_EL_SPD:
        pparam = &current_parameters.axis_man_limits[1].speed_max;
        param_lim_min = 0.5f;
        param_lim_max = 5.f;
        break;
    case PAR_EL_ACC:
        pparam = &current_parameters.axis_man_limits[1].accel;
        param_lim_min = 0.5f;
        param_lim_max = 5.f;
        break;
    case PAR_EL_DEC:
        pparam = &current_parameters.axis_man_limits[1].decel;
        param_lim_min = 0.5f;
        param_lim_max = 5.f;
        break;
    default:
        print_message(CONTROL_MESSAGE, "Unknown parameter code %d.\n",
                      in_data->parameter);
        ans_err_data* pans = out_data;
        pans->code = ANS_ERR;
        pans->err = ERR_NODATA;
        return sizeof(ans_err_data);
    }
    if(in_data->value > param_lim_max ||
       in_data->value < param_lim_min)
    {
        ans_err_data* pans = out_data;
        pans->code = ANS_ERR;
        pans->err = ERR_OP;
        return sizeof(ans_err_data);
    }
    *pparam = in_data->value;
    pap->parameter = in_data->parameter;
    pap->value = *pparam;
    pap->code = ANS_PAR;
    return sizeof(ans_par_data);
}

int set_azimuth_delta(cmd_sdir_data* pdata, const volatile struct drive_states* pstate)
{
    float real_azimuth = pos_dscrt_to_degree(pstate->axis[0].current_position);
    float real_elevation = pos_dscrt_to_degree(pstate->axis[1].current_position);
    current_parameters.azimuth_delta = pdata->azimuth - real_azimuth;
    current_parameters.elevation_delta = pdata->elevation - real_elevation;
    return 0;
}

bool process_incoming_command(const struct bup_data_block_header* input,
                              struct bup_data_block_header* output,
                              size_t outbuffer_size, struct work_thread* pdata)
{
    // sanity check
    if(input == NULL || output == NULL)
        return false;
    output->size = 0;
    uint8_t *pcmd_code = (uint8_t*)(input + 1);
    uint8_t *pans_code = (uint8_t*)(output + 1);
    bool res = false;
    switch(*pcmd_code)
    {
    case CMD_STOP:
    {
        print_message(CONTROL_MESSAGE, "Command %s recognized\n", "CMD_STOP");
        pthread_mutex_lock(&shs.commandlock);
        shs.bup_command = CMD_STOP;
        pthread_mutex_unlock(&shs.commandlock);
        output->size = 0;
    }
    break;
    case CMD_GTIME:
    {
        print_message(CONTROL_MESSAGE, "Command %s recognized\n", "CMD_GTIME");
        output->size = make_ans_time(pans_code);
    }
    break;
    case CMD_STIME:
    {
        print_message(CONTROL_MESSAGE, "Command %s recognized\n", "CMD_STIME");
        output->size = try_set_time((cmd_stime_data*)pcmd_code, pans_code,
                                    shs.current_state[1].operation_mode);
    }
    break;
    case CMD_STAT:
        print_message(CONTROL_MESSAGE, "Command %s recognized\n", "CMD_GETSTAT");
        output->size = send_stat(&shs.current_state[1], pans_code,
                                 shs.current_traj_data.data_mode,
                                 &shs.current_temperature);
        break;
    case CMD_MPNT:
    {
        print_message(CONTROL_MESSAGE, "Command %s recognized\n", "CMD_MPNT");
        output->size = move_to_position((cmd_mpnt_data*)pcmd_code, pans_code,
                                        shs.current_state[1].operation_mode);
    }
    break;
    case CMD_MMAN:
    {
        print_message(CONTROL_MESSAGE, "Command %s recognized\n", "CMD_MMAN");
        output->size = move_to_direction((cmd_mman_data*)pcmd_code, pans_code,
                                         shs.current_state[1].operation_mode);
    }
    break;
    case CMD_THEAD:
    {
        print_message(CONTROL_MESSAGE, "Command %s recognized\n", "CMD_THEAD");
        output->size = accept_new_trajectory((cmd_thead_data*)pcmd_code, pans_code,
                                             shs.current_state[1].operation_mode);
    }
    break;
    case CMD_TCONT:
    {
        print_message(CONTROL_MESSAGE, "Command %s recognized\n", "CMD_TCONT");
        output->size = recieve_points(input->size, pcmd_code, pans_code);
    }
    break;
    case CMD_RPAR:
    {
        print_message(CONTROL_MESSAGE, "Command %s recognized\n", "CMD_RPAR");
        output->size = send_param_value((cmd_rpar_data*)pcmd_code, pans_code);
    }
    break;
    case CMD_SPAR:
    {
        print_message(CONTROL_MESSAGE, "Command %s recognized\n", "CMD_SPAR");
        output->size = set_param_value((cmd_spar_data*)pcmd_code, pans_code);
        res = ((ans_err_data*)pans_code)->code != ANS_ERR;
    }
    break;
    case CMD_ALOG:
    {
        print_message(CONTROL_MESSAGE, "Command %s recognized\n", "CMD_ALOG");
        output->size = start_send_log(pans_code);
    }
    break;
    case CMD_RLOG:
    {
        print_message(CONTROL_MESSAGE, "Command %s recognized\n", "CMD_RLOG");
        output->size = send_more_log(outbuffer_size - sizeof(struct bup_data_block_header),
                                     pans_code);
    }
    break;
    case CMD_SDIR:
    {
        print_message(CONTROL_MESSAGE, "Command %s recognized\n", "CMD_SDIR");
        output->size = set_azimuth_delta((cmd_sdir_data*)pcmd_code, &shs.current_state[1]);
    }
    break;
    case CMD_DDIR:
    {
        print_message(CONTROL_MESSAGE, "Command %s recognized\n", "CMD_DDIR");
        current_parameters.azimuth_delta = 0;
        current_parameters.elevation_delta = 0;
    }
    break;
    case CMD_TDEL:
    {
        print_message(CONTROL_MESSAGE, "Command %s recognized\n", "CMD_TDEL");
        output->size = set_trajectory_delay((cmd_tdel_data*)pcmd_code, pans_code,
                                             shs.current_state[1].operation_mode);
    }
    break;
    default:
    {
        ans_err_data *pans = (ans_err_data*)pans_code;
        print_message(CONTROL_MESSAGE, "Command code %d not recognized.\n", *pcmd_code);
        pans->code = ANS_ERR;
        pans->err = ERR_FAIL;
        output->size = sizeof(ans_err_data);
    }
    break;
    }
    if(!output->size)
       output->size = send_stat(&shs.current_state[1], pans_code,
                                shs.current_traj_data.data_mode,
                                &shs.current_temperature);
    return res;
}

