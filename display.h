/** \file display.h
 * display data on screen
 */

#ifndef _DISPLAY_H_
#define _DISPLAY_H_

#include "global.h"
#include "trajectory.h"

#include <stdbool.h>
#include <string.h>


#define LOGPRINT print_log_message

enum message_types
{
    LOG_MESSAGE,
    CONTROL_MESSAGE,
    ACTIVE_MESSAGE
};

void print_log_message(char* format, ...);

/** Print message in corresponding section of the screen
 * \param type section identifyer
 * \param format same as \see printf
 */
void print_message(enum message_types type, char* format, ...);

void refresh_display();

/** set display mode
 * \param fullscreen true if ncurses library should be used
 * \param verbose number of data output
 */
void init_display(bool fullscreen, bool verbose);

void deinit_display();

/** Print interesting information from state structure
 * \param[in] pdata current state
 * \param[in] pts current trajetory states
 * \param[in] pfollowing current auto-tracking state
 * \param data_mode state of data transfers
 * \param[in] ptermo temperatures measured
 */
void display_state(const struct drive_states* pdata,
                   const struct trajectory_state* pts,
                   enum data_modes data_mode,
                   const struct termo_state* ptermo);

/** Makes string representation of data
 * \param[in] buffer data that should be represented
 * \param size number of bytes to represent
 * no more then 64
 * \returns pointer to zero-terminated string
 * \note function used a static buffer; it is overwritten after every use
 */
const char* make_hex_string(const unsigned char* buffer, size_t size);

#endif // _DISPLAY_H_
