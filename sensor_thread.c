#include "operations.h"
#include "bupprotocol.h"
#include "sensorprotocol.h"
#include "utils.h"
#include "display.h"
#include "assert.h"
#include "global.h"

#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <sys/time.h>
#include <float.h>

extern time_t day_start;
extern uint32_t defDPR;
extern struct trajectory_state current_traj_data;
extern struct all_parameters current_parameters;

#undef LOGPRINT
#define LOGPRINT if (0) print_log_message

void apply_limits(struct drive_states* pstate, const int32_t program_limits_pos[], const int32_t program_limits_neg[])
{
    // apply limits
    for(int i = 0; i < 2; ++i)
    {
        int axis_max_speed = spd_degree_to_dscrt(current_parameters.axis_limits[i].speed_max);
        // ensure that we always stay in defined bounds
        if(pstate->axis[i].max_speed > axis_max_speed)
        {
            pstate->axis[i].max_speed = axis_max_speed;
            print_message(ACTIVE_MESSAGE, "Max speed of axis %d capped at %d.\n", i + 1, axis_max_speed);
        }
        if(pstate->axis[i].target_position > program_limits_pos[i])
        {
            pstate->axis[i].target_position = program_limits_pos[i];
            // print_message(ACTIVE_MESSAGE, "Positive limit applied on axis %d.\n", i + 1);
        }
        if(pstate->axis[i].target_position < program_limits_neg[i])
        {
            pstate->axis[i].target_position = program_limits_neg[i];
            // print_message(ACTIVE_MESSAGE, "Negative limit applied on axis %d.\n", i + 1);
        }

        // check where we are at current point in time
        // invert endpoint signals
        pstate->axis[i].endpoint[0] = !(pstate->flag & left_endpoints[i]);
        pstate->axis[i].endpoint[1] = !(pstate->flag & right_endpoints[i]);
        pstate->axis[i].limit[0] = pstate->axis[i].current_position <= program_limits_neg[i];
        pstate->axis[i].limit[1] = pstate->axis[i].current_position >= program_limits_pos[i];
        
        if(pstate->axis[i].zprivod < 0 &&
           (pstate->axis[i].endpoint[0] || pstate->axis[i].limit[0]))
        {
            pstate->axis[i].zprivod = 0;
            print_message(ACTIVE_MESSAGE, "Negative movement of axis %d stopped by endpoint/limit.\n", i + 1);
        }

        if(pstate->axis[i].zprivod > 0 &&
           (pstate->axis[i].endpoint[1] || pstate->axis[i].limit[1]))
        {
            pstate->axis[i].zprivod = 0;
            print_message(ACTIVE_MESSAGE, "Positive movement of axis %d stopped by endpoint/limit.\n", i + 1);
        }
    }
}

void control_system_pid(struct drive_states* pstate, double azimuth_speed, double elevation_speed)
{
    double speed_component[2] = { azimuth_speed, elevation_speed };
    /* print_message(ACTIVE_MESSAGE, "Again: Azimuth speed: %f; elevation speed: %f\n", */
    /*               speed_component[0], speed_component[1]); */

    int error_component[2] = { 0, 0 };
    static int integral_component[2] = { 0, 0 };
    
    long feedback_component[2] = { 0, 0 };
    
    for(int i = 0; i < 2; ++i)
    {
        struct axis_state* paxis = pstate->axis + i;
        float speed = 0;
        feedback_component[i] = paxis->current_position - paxis->prev_position;
        error_component[i] = paxis->target_position - paxis->current_position;
        error_component[i] = fabs(error_component[i]) > current_parameters.DPR?
            fsign(error_component[i]) * current_parameters.DPR: error_component[i];

        integral_component[i] += error_component[i];
        integral_component[i] = fabs(integral_component[i]) > 2000? fsign(integral_component[i]) * 2000: integral_component[i];

        speed = error_component[i] * current_parameters.K1[i] + integral_component[i] * current_parameters.KI[i] +
            speed_component[i] * current_parameters.KK[i] - feedback_component[i] * current_parameters.KS[i];
        // paxis->y4 = error_component[i] * current_parameters.K1[i];
        paxis->zprivod = lrintf(current_parameters.KZ2D * speed);
        if(abs(paxis->zprivod) > paxis->max_speed)
        {
            print_message(ACTIVE_MESSAGE, "Speed capped from %d to %d on axis %d\n",
                          paxis->zprivod, paxis->max_speed, i);
            paxis->zprivod = sign(paxis->zprivod) * paxis->max_speed;
            
        }
        else if (abs(paxis->zprivod) < paxis->min_speed)
            paxis->zprivod = 0;
        // TODO: add privod stop counter
        paxis->prev_position = paxis->current_position;
        print_message(ACTIVE_MESSAGE, "Tracking axis %d: Zprivod = %d, target = %d, current = %d\n",
                      i, paxis->zprivod, paxis->target_position, paxis->current_position);

    }
}

void control_system_position(struct drive_states* pstate)
{
    for(int i = 0; i < 2; ++i)
    {
        struct axis_state* paxis = pstate->axis + i;
        if(paxis->start)
        {
            print_message(ACTIVE_MESSAGE, "Starting axis %d.\n", i+1);
            paxis->prev_position = paxis->current_position - 10 * sign(paxis->target_position - paxis->current_position);
            paxis->accelerate = true;
            paxis->start = false;
            // switch on i?..
        }

        if(abs(paxis->target_position - paxis->current_position) > abs(paxis->zprivod/current_parameters.K1[i]) &&
           paxis->accelerate)
        {
            print_message(ACTIVE_MESSAGE, "Before acceleration of axis %d: "
                          "zprivod: %d, current_position: %d, target_position:%d.\n",
                          i+1, paxis->zprivod, paxis->current_position, paxis->target_position);
            paxis->zprivod = lrintf(current_parameters.K1[i] * current_parameters.KZ2D *
                                    (paxis->current_position - paxis->prev_position));
            print_message(ACTIVE_MESSAGE, "Accelerating axis %d; zprivod: %d.\n", i+1, paxis->zprivod);

        }
        else
        {
            if(!paxis->stopped)
                print_message(ACTIVE_MESSAGE, "Before deceleration of axis %d: "
                              "zprivod: %d, current_position: %d, target_position:%d.\n",
                              i+1, paxis->zprivod, paxis->current_position, paxis->target_position);
            paxis->zprivod = lrintf(current_parameters.K1[i] * current_parameters.KZ2D *
                                    (paxis->target_position - paxis->current_position));
            paxis->accelerate = false;

            if(!paxis->stopped)
                print_message(ACTIVE_MESSAGE, "Decelerated axis %d; zprivod: %d.\n", i+1, paxis->zprivod);
        }

        if(abs(paxis->zprivod) > paxis->max_speed)
        {
            paxis->zprivod = sign(paxis->zprivod) * paxis->max_speed;
        }
        else if (abs(paxis->zprivod) < paxis->min_speed)
        {
            paxis->zprivod = 0;
        }

        if(!paxis->stopped)
        {
            print_message(ACTIVE_MESSAGE, "Zprivod = %d, max_speed = %d, min_speed = %d\n",
                          paxis->zprivod, paxis->max_speed, paxis->min_speed);
        }
    }
}

int send_stat(const volatile struct drive_states* pstate,
              void* pdata, enum data_modes data_mode,
              struct termo_state *ptemp)
{
    ans_stat_data* panswer = (ans_stat_data*)pdata;
    panswer->code = ANS_STAT;

    // convert axis coordinates
    panswer->az_speed = pstate->axis[0].real_speed;
    // spd_dscrt_to_degree(pstate->axis[0].zprivod);
    panswer->azimuth = pos_dscrt_to_degree(pstate->axis[0].current_position) +
        current_parameters.azimuth_delta;
    panswer->el_speed = pstate->axis[1].real_speed;
    // spd_dscrt_to_degree(pstate->axis[1].zprivod);
    panswer->elevation = pos_dscrt_to_degree(pstate->axis[1].current_position) +
        current_parameters.elevation_delta;

    // endpoint flags
    panswer->end_flags = 0;
    if(pstate->axis[0].endpoint[0])
        panswer->end_flags |= AXIS1_LHE;
    if(pstate->axis[0].endpoint[1])
        panswer->end_flags |= AXIS1_RHE;
    if(pstate->axis[1].endpoint[0])
        panswer->end_flags |= AXIS2_LHE;
    if(pstate->axis[1].endpoint[1])
        panswer->end_flags |= AXIS2_RHE;

    // axis error/readiness flags
    panswer->system_flags = 0;
    if(pstate->axis[0].ready)
        panswer->system_flags |= AXIS1_RDY;
    if(pstate->axis[0].alarm)
        panswer->system_flags |= AXIS1_ERR;;
    if(pstate->axis[1].ready)
        panswer->system_flags |= AXIS2_RDY;
    if(pstate->axis[1].alarm)
        panswer->system_flags |= AXIS2_ERR;

    // temperature data
    panswer->temp1 = 0.f;
    panswer->temp2 = 0.f;
    if(ptemp)
    {
        if(ptemp->sensor[0])
            panswer->end_flags |= TEMP1;
        if(ptemp->sensor[1])
            panswer->end_flags |= TEMP2;
        if(ptemp->heating[0])
            panswer->end_flags |= HEAT1;
        if(ptemp->heating[1])
            panswer->end_flags |= HEAT2;
        panswer->temp1 = ptemp->temperature[0];
        panswer->temp2 = ptemp->temperature[1];
    }
    
    // trajectory flags
    if(pstate->operation_mode == mTracking)
        panswer->system_flags |= FLW_TRAJ;
    else if(pstate->operation_mode == mWaitStartTime)
        panswer->system_flags |= WAIT_TRAJ;

    // data transfer flags
    if(data_mode == dmRecieving)
        panswer->system_flags |= RCV_TRAJ;
    else if(data_mode == dmSending)
        panswer->system_flags |= SEND_LOG;

    return sizeof(ans_stat_data);
}

/* void load_track_header(struct trajectory_header* ptrack, pthread_mutex_t* plock, const struct TLoadTrkHdr* pdata) */
/* { */
/*     if(ptrack == NULL) */
/*         return; */

/*     pthread_mutex_lock(plock); */

/*     ptrack->first_point = pdata->FstPnt; */
/*     // time is recieved in msec */
/*     ptrack->start_time = day_start + (pdata->StartTime / 1000); */

/*     reinit_trajectory(ptrack); */
/*     struct trajectory_point* point[3]; */
/*     point[0] = get_write_point(ptrack, pdata->FstPnt + 0); */
/*     point[1] = get_write_point(ptrack, pdata->FstPnt + 1); */
/*     point[2] = get_write_point(ptrack, pdata->FstPnt + 2); */

/*     const struct TFstPnt* pK[] = { &pdata->K1, &pdata->K2, &pdata->K3 }; */

/*     // order in incoming packet is messed up */
/*     // and is nonsensical */
/*     for(int i = 0; i < 3; ++i) */
/*     { */
/*         for(int j = 0; j < 2; ++j) */
/*         { */
/*             point[i]->axis_pos[j] = pK[j]->Pnt[i]; */
/*         } */
/*         print_message(CONTROL_MESSAGE, "Point %d: (%f, %f, %f)", i+1, */
/*                       pK[0]->Pnt[i], pK[1]->Pnt[i], pK[2]->Pnt[i]); */
/*                       // point[i]->axis_pos[0], point[i]->axis_pos[1], point[i]->axis_pos[2]); */
                  

/*     } */
    
/*     pthread_mutex_unlock(plock); */
/* } */

/* void add_track_point(struct trajectory_header* ptrack, pthread_mutex_t* plock, const struct TLoadTrackPnt* pdata, uint16_t point_number) */
/* { */
/*     if(ptrack == NULL) */
/*         return; */

/*     pthread_mutex_lock(plock); */
/*     struct trajectory_point* current = get_write_point(ptrack, point_number); */
/*     current->axis_pos[0] = pdata->K1; */
/*     current->axis_pos[1] = pdata->K2; */
/*     // current->axis_pos[2] = pdata->K3; */

/*     print_message(CONTROL_MESSAGE, "Next point: #%d (%f, %f)", */
/*                   point_number, pdata->K1, pdata->K2); */

/*     pthread_mutex_unlock(plock); */
/* } */

extern uint32_t defSPDMAX;
extern uint32_t defSPDMIN;

int16_t reverse_zprivod(int16_t zprivod)
{
    /*
     * Formula for the frequency from the intermediate controller is
     *
     * Fout = Fclk / (2 * N * (1 + OCRnA))
     *
     * N == 1 : prescaler factor
     * Fclc == 11059200 Hz : quartz clock frequency
     *
     * minimum ZPrivod is 11?
     */
    const int Fclk = 11059200;
    int result = 0;
    // 0 is special case; it means stopping the privod
    if(zprivod == 0 || abs(zprivod) < current_parameters.SPDMIN)
        return 0;
    if(abs(zprivod) > current_parameters.SPDMAX)
        zprivod = sign(zprivod) * current_parameters.SPDMAX;

    // from pulses on servoamplifier to pulses output by timer
    // zprivod *= 2;
    
    if(zprivod > 0)
    {
        result = (Fclk / (zprivod << 1)) - 1;
    }
    else
    {
        zprivod = -zprivod;
        // for negative values, flip around
        result = 1 - (Fclk / (zprivod << 1));
    }

    // asserts!
    assert((result >= 99 && result < INT16_MAX) ||
           (result <= -99 && result > INT16_MIN));
    return result;
}

//bup_interaction(&current_drive_state, &command, &alarm_counter, data->afd)
bool bup_interaction(struct drive_states* current_state,
                     struct bup_data_block_set* command,
                     int* alarm_counter, int afd,
                     unsigned char* inbuffer, int inbuffer_size)
{
    const int alarm_count = 100;
    unsigned char buffer[30];

    if(!current_state || !command || !inbuffer)
        return false;

    memset(buffer, 0, sizeof(buffer));
    memset(inbuffer, 0, inbuffer_size);

    struct bup_data_block_header* pheader = (struct bup_data_block_header*) buffer;
    pheader->size = sizeof(struct bup_data_block_set);

    // Temporary: for frequency checking!
    // current_state->axis[0].zprivod = -1500;

    command->cmd_flags = CMDP_OUT0;
    command->cmd_code = CMD_SET;
    uint16_t narrow = 0;
    enum bup_directive_flags minus_flags[] = {
        CMDP_CH0_USEA,
        CMDP_CH1_USEA,
        CMDP_CH2_USEA,
        CMDP_CH3_USEA
    };
    for(int i = 0; i < 2; ++i)
    {
        // in real world, value should never go that far
        narrow = abs(current_state->axis[i].zprivod) > UINT16_MAX? UINT16_MAX: abs(current_state->axis[i].zprivod);
        command->cmd_freq[i] = narrow;
        // take sign into accout
        // A for positive direction
        if(current_state->axis[i].zprivod * current_parameters.AmplifierDirectionFlags[i] > 0)
            command->cmd_flags |= minus_flags[i];
    }
    // calculate crc
    memcpy(buffer + sizeof(struct bup_data_block_header), command, pheader->size);

    write_bup_data(pheader, afd);
    // discard all sensors data recieved during the wait
    // tcdrain(afd);

    // print_message(LOG_MESSAGE, "Packet sent: %s\n", make_hex_string((unsigned char*)(&command), sizeof(command)));
    LOGPRINT("Speeds: %d; %d;\n",
             command->cmd_freq[0], command->cmd_freq[1]);
    // TODO: read response

    if(recieve_bup_data(inbuffer, inbuffer_size, afd, NULL))
    {
        LOGPRINT("Recieved response from BUP.\n");
        const struct bup_data_block_set_response* pdata = (struct bup_data_block_set_response*)(inbuffer + sizeof(struct bup_data_block_header));
        process_state_flags(current_state, pdata->cmd_flags); 
    }
    else
        LOGPRINT("Could not get response from BUP.\n");
#ifdef BUP_STATUS
    if(alarm_counter != NULL)
    {
        if(!any_alarm(current_state))
        {
            *alarm_counter = 0;
            if(current_state->operation_mode == mAlarm)
                current_state->operation_mode = mIdle;
        }
        else
            (*alarm_counter)++;

        if(*alarm_counter > alarm_count)
        {
            LOGPRINT("Persistent error state on servo amplifier!\n");
            current_state->operation_mode = mAlarm;
            return false;
        }
    }
#endif //BUP_STATUS
   return true;
}

void perform_tracking(const int32_t points_short_buffer[2*2],
                      int32_t time_short_buffer[3],
                      int32_t current_time,
                      struct drive_states* pstate)
{
    // speeds in trajectory
    double current_azimuth_speed = 0.0, current_elevation_speed = 0.0;
    // straighten the arrow of time
    float dt2 = time_short_buffer[2] - time_short_buffer[1];
    if(dt2 < 1.f)
        dt2 = 1.f;
    float dt1 = time_short_buffer[1] - time_short_buffer[0];
    if(dt1 < 1.f)
        dt1 = 1.f;
    float ntime = current_time - time_short_buffer[0];

    for(int j = 0; j < 2; ++j)
    {
        // a = (point#2 - point#1)/(dt1 + dt2)*dt2 - (point#1 - point#0)/dt1 * dt2
        double a = ((points_short_buffer[2*2 + j] - points_short_buffer[2*0 + j]) / (dt1 + dt2) -
                    (points_short_buffer[2*1 + j] - points_short_buffer[2*0 + j])/dt1) / dt2;
        // b = (point#1 - point#0)/dt1 - a * dt1
        double b = (points_short_buffer[2*1 + j] - points_short_buffer[2*0 + j]) / dt1 - a * dt1;

        int anpos = lrint((a * ntime + b) * ntime + points_short_buffer[2*0 + j]);
        // auto search component
        pstate->axis[j].target_position = anpos;

        double anspeed = 2.0 * a * ntime + b;
        if(j == 1)
        {
            current_elevation_speed = anspeed * 1000.f;
        }
        if(j == 0)
        {
            current_azimuth_speed = anspeed * 1000.f;
        }

        print_message(ACTIVE_MESSAGE, "Axis %d point: base: %d, calculated: %d.\n",
                      j, points_short_buffer[2*0 + j], pstate->axis[j].target_position);
        print_message(ACTIVE_MESSAGE, "Timedelta: %f, a: %f, b:%f, next: %d.\n",
                      ntime, a, b, points_short_buffer[2*1 + j]);
    }
    print_message(ACTIVE_MESSAGE, "Azimuth speed: %f; elevation speed: %f\n",
                  current_azimuth_speed, current_elevation_speed);
    control_system_pid(pstate, current_azimuth_speed, current_elevation_speed);
    // control_system_position(pstate);
}

void control_system_speed(struct drive_states* pstate, enum direction_codes dir)
{
    // determine needs to stop
    int terminal_distances[2];
    int target_speeds[2];
    float deg_delta;
    for(int i = 0; i < 2; ++i)
    {
        terminal_distances[i] = current_parameters.axis_man_limits[i].speed_max *
            current_parameters.axis_man_limits[i].speed_max / (2 * current_parameters.axis_man_limits[i].decel);
    }
    // determine speeds by direction
    switch(dir)
    {
    case DIR_STOP:
        target_speeds[0] = 0;
        target_speeds[1] = 0;
        break;
    case DIR_AZ_POS:
        
        target_speeds[1] = 0;
        deg_delta = current_parameters.axis_limits[0].limit_pos - pos_dscrt_to_degree(pstate->axis[0].current_position);
        if(fabs(deg_delta) < terminal_distances[0])
        {
            target_speeds[0] = spd_degree_to_dscrt(sqrtf(fabs(deg_delta) *
                                                         current_parameters.axis_man_limits[0].decel));
            print_message(ACTIVE_MESSAGE, "Azimuth at %f from endpoint; Target speed %f\n",
                          deg_delta, target_speeds[0]);
        }
        else
            target_speeds[0] = spd_degree_to_dscrt(current_parameters.axis_man_limits[0].speed_max);
        break;
    case DIR_AZ_NEG:

        target_speeds[1] = 0;
        deg_delta = pos_dscrt_to_degree(pstate->axis[0].current_position) - current_parameters.axis_limits[0].limit_neg;
        if(fabs(deg_delta) < terminal_distances[0])
        {
            target_speeds[0] = -1 * spd_degree_to_dscrt(sqrtf(fabs(deg_delta) *
                                                              current_parameters.axis_man_limits[0].decel));
            print_message(ACTIVE_MESSAGE, "Azimuth at %f from endpoint; Target speed %f\n",
                          deg_delta, target_speeds[0]);
        }
        else
            target_speeds[0] = spd_degree_to_dscrt(-1 * current_parameters.axis_man_limits[0].speed_max);
        break;
    case DIR_EL_POS:
        target_speeds[0] = 0;
        deg_delta = current_parameters.axis_limits[1].limit_pos - pos_dscrt_to_degree(pstate->axis[1].current_position);
        if(fabs(deg_delta) < terminal_distances[1])
        {
            target_speeds[1] = spd_degree_to_dscrt(sqrtf(fabs(deg_delta) *
                                                         current_parameters.axis_man_limits[1].decel));
            print_message(ACTIVE_MESSAGE, "Elevation at %f from endpoint; Target speed %f\n",
                          deg_delta, target_speeds[1]);
        }
        else
            target_speeds[1] = spd_degree_to_dscrt(current_parameters.axis_man_limits[1].speed_max);
        break;
    case DIR_EL_NEG:
        target_speeds[0] = 0;
        deg_delta = pos_dscrt_to_degree(pstate->axis[1].current_position) - current_parameters.axis_limits[1].limit_neg;
        if(fabs(deg_delta) < terminal_distances[1])
        {
            target_speeds[1] = -1 * spd_degree_to_dscrt(sqrtf(fabs(deg_delta) *
                                                              current_parameters.axis_man_limits[1].decel));
            print_message(ACTIVE_MESSAGE, "Elevation at %f from endpoint; Target speed %f\n",
                          deg_delta, target_speeds[1]);
        }
        else
            target_speeds[1] = spd_degree_to_dscrt(-1 * current_parameters.axis_man_limits[1].speed_max);
        break;
    }
    // determine acceleration to use
    float delta_speeds[2];
    for(int i = 0; i < 2; ++i)
    {
        if(target_speeds[i] * pstate->axis[i].zprivod < 0 ||
           abs(target_speeds[i]) < abs(pstate->axis[i].zprivod))
            delta_speeds[i] = current_parameters.axis_man_limits[1].decel;
        else
            delta_speeds[i] = current_parameters.axis_man_limits[1].accel;
        // take consideration of the speed change distribution over the second
        delta_speeds[i] = spd_degree_to_dscrt(delta_speeds[i]) / FREQUENCY;
        delta_speeds[i] *= (target_speeds[i] - pstate->axis[i].zprivod > 0)? 1: -1;
    }
    // apply speed change
    for(int i = 0; i < 2; ++i)
    {
        if(abs(target_speeds[i] - pstate->axis[i].zprivod) < abs(delta_speeds[i]))
            pstate->axis[i].zprivod = target_speeds[i];
        else
            pstate->axis[i].zprivod += delta_speeds[i];
        pstate->axis[i].stopped = pstate->axis[i].zprivod == 0;
    }
}

void process_state_flags(struct drive_states* pstate, uint8_t flags)
{
    if(pstate == NULL)
        return;

    pstate->axis[0].ready = flags & CMDP_IN0;
    pstate->axis[0].alarm = !(flags & CMDP_IN3);
    pstate->axis[1].ready = flags & CMDP_IN1;
    pstate->axis[1].alarm = !(flags & CMDP_IN4);
}

void compute_real_speed(struct axis_state* pstate, suseconds_t cur_msec)
{
    // sanity!
    pstate->speed_cursor = pstate->speed_cursor % AXIS_SPEED_BUFFER_SIZE;
    int prev_time = pstate->prev_times_msec[pstate->speed_cursor];
    int prev_pos = pstate->old_positions[pstate->speed_cursor];
    int sum_delta = 0, sum_period = 0;
    for(int i = pstate->speed_cursor+1; i % AXIS_SPEED_BUFFER_SIZE != pstate->speed_cursor; ++i)
    {
        sum_delta += pstate->old_positions[i % AXIS_SPEED_BUFFER_SIZE];
        sum_period += pstate->prev_times_msec[i % AXIS_SPEED_BUFFER_SIZE];
    }
    if(prev_time == 0 || cur_msec < prev_time)
    {
        pstate->real_speed = 0.f;
        pstate->old_positions[pstate->speed_cursor] = 0;
        pstate->prev_times_msec[pstate->speed_cursor] = 0;
    }
    else
    {
        pstate->old_positions[pstate->speed_cursor] = labs(pstate->current_position - prev_pos);
        sum_delta += pstate->old_positions[pstate->speed_cursor];
        pstate->prev_times_msec[pstate->speed_cursor] = cur_msec - prev_time;
        sum_period += pstate->prev_times_msec[pstate->speed_cursor];

        if(sum_period > 0)
            pstate->real_speed = pos_dscrt_to_degree(1000 * sum_delta) / sum_period;
        else
            pstate->real_speed = 0.f;
    }
    pstate->speed_cursor = (1 + pstate->speed_cursor) % AXIS_SPEED_BUFFER_SIZE;
    pstate->old_positions[pstate->speed_cursor] = pstate->current_position;
    pstate->prev_times_msec[pstate->speed_cursor] = cur_msec;
}

bool drive_state_short_circuit(int ptty_active, struct drive_states* pstate,
                               bool(*tester)(const struct drive_states* pstate))
{
    LOGPRINT("Waiting for BUP state changes\n");
    // command block is set to all zeros: in short circuited state we ask drives to stand still
    struct bup_data_block_set command;
    memset(&command, 0, sizeof(struct bup_data_block_set));
    // hold servo-amplifier in ON state
    command.cmd_flags = CMDP_OUT0;
    command.cmd_code = CMD_SET;
    unsigned char buffer[30], inbuffer[30];
    memset(buffer, 0, sizeof(buffer));
    memset(inbuffer, 0, sizeof(inbuffer));
    if(!pstate || ptty_active == -1)
        return false;
    struct bup_data_block_header* pheader = (struct bup_data_block_header*) buffer;
    pheader->blockstart = bup_block_start;
    pheader->size = sizeof(struct bup_data_block_set);
    memcpy(buffer + sizeof(struct bup_data_block_header), &command, pheader->size);
    pheader->crc = calculate_crc8(buffer + sizeof(struct bup_data_block_header), pheader->size);
    int counter = 0;
    while(!shs.done)
    {
        pstate->time_sec = time(NULL);
        write(ptty_active, buffer, sizeof(struct bup_data_block_header) + pheader->size);
        if(recieve_bup_data(inbuffer, sizeof(inbuffer), ptty_active, &counter))
        {
            // LOGPRINT("Recieved packet from BUP.\n");
            const struct bup_data_block_set_response* pdata = (struct bup_data_block_set_response*)(inbuffer + sizeof(struct bup_data_block_header));
            process_state_flags(pstate, pdata->cmd_flags);
            update_inner_stat(pstate);
            if(!(*tester)(pstate))
            {
                LOGPRINT("BUP state changed\n");
                return true;
            }
            
        }
        else
        {
            // debug output here
            LOGPRINT("Did not recognize data from BUP; ");
            LOGPRINT("Data recieved (%d bytes): %s\n", counter,  make_hex_string(inbuffer, counter));            
        }
    }
    return false;
}

bool not_ready(const struct drive_states* pstate)
{
    if(!pstate)
        return true;
    for(int i = 0; i < 2; ++i)
        if(!pstate->axis[i].ready)
            return true;
    return false;
}

void* threadfunc(void* arg)
{
   struct work_thread* data = arg;
   struct drive_states current_drive_state;
   memset(&current_drive_state, 0, sizeof(struct drive_states));

   // setup buffer for incoming sensor data
   unsigned char inbuffer[255];
   memset(inbuffer, 0, 255);
   unsigned char inbuffer2[50];
   memset(inbuffer2, 0, 50);

   struct sensors_data_block* pblock = (struct sensors_data_block*)inbuffer;
    
   // outgoing command
   struct bup_data_block_set command;
   memset(&command, 0, sizeof(struct bup_data_block_set));

   // trajectory  points microbuffer
   int32_t trajectory_3_points[2*3];
   memset(trajectory_3_points, 0, sizeof(trajectory_3_points));
   int32_t trajectory_3_times[3];
   /// elevation and azimuth correction in descreets
   int16_t elevation_correction = 0, azimuth_correction = 0;

   /// counters for determining if drives are stopped
   int stop_counters[2] = {0, 0};
   const int stop_count = 200;
   /// signal level holder
   int signal_level = 0;
   /// counter for alarm state
   int alarm_counter = 0;
   /// program limits in discreets
   int32_t program_limits_pos[2], program_limits_neg[2];
   for(int i = 0; i < 2; ++i)
   {
      program_limits_pos[i] = pos_degree_to_dscrt(current_parameters.axis_limits[i].limit_pos);
      program_limits_neg[i] = pos_degree_to_dscrt(current_parameters.axis_limits[i].limit_neg);
      current_drive_state.axis[i].min_speed = spd_degree_to_dscrt(current_parameters.axis_limits[i].speed_min);
      current_drive_state.axis[i].max_speed = spd_degree_to_dscrt(current_parameters.axis_limits[i].speed_max);
      print_message(ACTIVE_MESSAGE, "Axis %d limits (degree): positive = %f, negative=%f.\n", i + 1,
                    current_parameters.axis_limits[i].limit_pos,
                    current_parameters.axis_limits[i].limit_neg);
      print_message(ACTIVE_MESSAGE, "Axis %d limits: positive = %d, negative=%d.\n", i + 1,
                    program_limits_pos[i], program_limits_neg[i]);
   }

   // allow the process to recieve SIGIO
   fcntl(data->dfd, F_SETOWN, getpid());
    
   sigset_t everything;
   //sigemptyset(&interrupt);
   sigfillset(&everything);
   pthread_sigmask(SIG_UNBLOCK, &everything, NULL);

   data->total = 0;

   time_t cur_sec = time(NULL);	///<- time in seconds for counting number of packets in second
   suseconds_t trajectory_msec = 0;	///<- time in milliseconds used for trajectory point requests
   int iter= 0;

   struct timeval begin_op, end_op;///<- timestamps for calculating duration of the cycle
   bool packet_recieved = false;

   // wait interval is set to half of a millisecond
   struct timeval wait_interval = {0, 500};

// #undef BUP_STATUS
#ifdef BUP_STATUS
   // ensure drives are ready before start
   current_drive_state.operation_mode = mNotReady;
   // propagate readiness flags
   update_inner_stat(&current_drive_state);
   // wait for drive readiness
   drive_state_short_circuit(data->afd, &current_drive_state, &not_ready);
#endif // BUP_STATUS
   current_drive_state.operation_mode = mIdle;
   // propagate readiness flags
   update_inner_stat(&current_drive_state);
   // discard all sensors data recieved during the wait
   tcflush(data->dfd, TCIOFLUSH);
    
   while(!shs.done)
   {
      gettimeofday(&begin_op, NULL);
      // in case temp is less, time has changed and needs to be reset
      if(begin_op.tv_sec != cur_sec)
      {
         cur_sec = begin_op.tv_sec;
         print_message(ACTIVE_MESSAGE, "Recieved %d packets in 1 second.\n", iter);
         iter = 0;
      }
      // msec current time
      trajectory_msec = (begin_op.tv_sec - day_start) * 1000 + begin_op.tv_usec / 1000;
      /* if(program_limits_flag) */
      /*    update_limits_inner(&current_drive_state, program_limits_pos, program_limits_neg); */

      current_drive_state.time_sec = begin_op.tv_sec;

      packet_recieved = parse_input_data(data->dfd,
                                         &current_drive_state, &data->total,
                                         inbuffer, 255);

      iter += packet_recieved;
      shs.input = packet_recieved;


      if(shs.done) break;
      //}
      // in any case, send command
      memset(&command, 0, sizeof(struct bup_data_block_set));
      if(shs.bup_command != CMD_COUNT)
      {
         if(!pthread_mutex_trylock(&shs.commandlock))
         {
            if(current_drive_state.operation_mode != mAlarm)
            {
               print_message(ACTIVE_MESSAGE, "Executing command code %d\n", shs.bup_command);
               switch(shs.bup_command)
               {
               case CMD_STOP:
                   current_drive_state.operation_mode = mIdle;
                   print_message(ACTIVE_MESSAGE, "Switching to idle on command.\n");
                   break;
               case CMD_MPNT:
               {
                   current_drive_state.operation_mode = mMovePnt;
                   for(int i = 0; i < 2; ++i)
                   {
                       current_drive_state.axis[i].target_position = pos_degree_to_dscrt(shs.target_position.position[i]);
                       current_drive_state.axis[i].max_speed = spd_degree_to_dscrt(shs.target_speed.speed[i]);
                       current_drive_state.axis[i].start = true;
                       current_drive_state.axis[i].stopped = false;
                       current_drive_state.axis[i].inp = false;
                       stop_counters[i] = 0;
                       print_message(ACTIVE_MESSAGE, "Target for axis %d is %d.\n",
                                     i + 1, current_drive_state.axis[i].target_position);
                   }
               }
               break;
               case CMD_MMAN:
               {
                   current_drive_state.operation_mode = mMoveMan;
                   for(int i = 0; i < 2; ++i)
                   {
                       current_drive_state.axis[i].max_speed = spd_degree_to_dscrt(current_parameters.axis_man_limits[i].speed_max);
                   }
                   print_message(ACTIVE_MESSAGE, "Moving axes in direction number #%d\n",
                                 shs.target_direction);

               }
               break;
               case CMD_THEAD:
               {
                   // set destination point to first trajectory point
                   current_drive_state.operation_mode = mWaitStartTime;
                   // load first point
                   const struct trajectory_point* ppoint = get_read_point(&shs.current_traj_data.trajectory, 0);
                   memset(trajectory_3_points, 0, sizeof(trajectory_3_points));
                   for(int j = 0; j < 2; ++j)
                   { trajectory_3_points[j] = pos_degree_to_dscrt(ppoint->axis_pos[j]); }
                   trajectory_3_times[0] = (shs.current_traj_data.trajectory.start_time - day_start) * 1000;
                   for(int i = 0; i < 2; ++i)
                   {
                       current_drive_state.axis[i].stopped = false;
                       stop_counters[i] = 0;
                       current_drive_state.axis[i].inp = false;
                       current_drive_state.axis[i].target_position = trajectory_3_points[i];
                       shs.target_position.position[i] = ppoint->axis_pos[i];
                       current_drive_state.axis[i].max_speed = spd_degree_to_dscrt(current_parameters.axis_limits[i].speed_max);
                       print_message(ACTIVE_MESSAGE, "Axis %d max speed set to %d (%f)\n",
                                     i, current_drive_state.axis[i].max_speed,
                                     current_parameters.axis_limits[i].speed_max);
                   }
                   print_message(ACTIVE_MESSAGE, "Trajectory point #0: [%d, %d]\n",
                                 trajectory_3_points[0], trajectory_3_points[1]);
                   shs.current_traj_data.trajectory_cursor = 0;
               }
               break;
               default:
                   break;
               };
            } // if !mAlarm
            // we send command only once
            shs.bup_command = CMD_COUNT;
            pthread_mutex_unlock(&shs.commandlock);
         } // mutex lock
      } // shs.bup_command != CMD_COUNT
      else
      {
         // check for full stop
         bool full_stop = true;
         for(int i = 0; i < 2; ++i)
         {
            full_stop = full_stop && current_drive_state.axis[i].stopped;
         }
         if(full_stop && (current_drive_state.operation_mode != mWaitStartTime)
            && (current_drive_state.operation_mode != mIdle)
            && (current_drive_state.operation_mode != mTracking))
         {
            current_drive_state.operation_mode = mIdle;
            print_message(ACTIVE_MESSAGE, "Switching to idle.\n");
         }
      }
      // determine speeds
      switch(current_drive_state.operation_mode)
      {
      case mAlarm:
      case mIdle:
      {
          // set all speeds to 0
          for(int i = 0; i < 2; ++i)
          {
              current_drive_state.axis[i].target_position = current_drive_state.axis[i].current_position;
              current_drive_state.axis[i].zprivod = 0;
              current_drive_state.axis[i].max_speed = current_parameters.axis_limits[i].speed_max;
              current_drive_state.axis[i].inp = true;
          }
      }
      break;
      case mMovePnt:
      {
#ifdef POINT_NOT_SIMPLE
          control_system_pid(&current_drive_state, 0, 0);
#else
          control_system_position(&current_drive_state);
#endif
          for(int i = 0; i < 2; ++i)
              if(current_drive_state.axis[i].stopped)
                  current_drive_state.axis[i].inp = true;
      }
      break;
      case mMoveMan:
      {
          control_system_speed(&current_drive_state, shs.target_direction);
          for(int i = 0; i < 2; ++i)
              if(current_drive_state.axis[i].stopped)
                  current_drive_state.axis[i].inp = true;
              else
                  stop_counters[i] = 0;
      }
      break;
      case mWaitStartTime:
      {
          int temp_deci = 10 * shs.current_traj_data.trajectory.start_time +
              shs.current_traj_data.trajectory.deciseconds +
              shs.current_traj_data.trajectory.start_delta;
          int cur_deci = cur_sec * 10 + ((trajectory_msec % 1000) / 100);

          control_system_position(&current_drive_state);
    
          // check time
          /* if(cur_sec >= (shs.current_traj_data.trajectory.start_time + */
          /*                shs.current_traj_data.trajectory.start_delta) && */
          /*    (trajectory_msec % 1000) >= shs.current_traj_data.trajectory.deciseconds) */
          if (cur_deci >= temp_deci)
          {
              struct trajectory_header *ptraj = &shs.current_traj_data.trajectory;
              {
                  int hour, min, sec, thour, tmin, tsec;
                  hour = (cur_sec - day_start) / 3600;
                  min = ((cur_sec - day_start) % 3600) / 60;
                  sec = (cur_sec - day_start) % 60;
                  thour = (ptraj->start_time + (ptraj->start_delta / 10) - day_start) / 3600;
                  tmin = ((ptraj->start_time + (ptraj->start_delta / 10) - day_start) % 3600) / 60;
                  tsec = (ptraj->start_time + (ptraj->start_delta / 10) - day_start) % 60;
                  print_message(LOG_MESSAGE,
                                "Tracking started at %02d:%02d:%02d (trajectory start at %02d:%02d:%02d)\n",
                                hour, min, sec, thour, tmin, tsec);
              }
              current_drive_state.operation_mode = mTracking;
              // set current position as first point in trajectory buffer
              trajectory_3_points[0] = current_drive_state.axis[0].current_position;
              trajectory_3_points[1] = current_drive_state.axis[1].current_position;
              trajectory_3_times[0] = trajectory_msec;
              // immediately prepare 2 next points
              for(int i = 1; i < 3; ++i)
              {
                  const struct trajectory_point* ptp = get_read_point(ptraj,
                                                                      shs.current_traj_data.trajectory_cursor + i);
                  if(ptp != NULL)
                  {
                      for(int j = 0; j < 2; ++j)
                      { trajectory_3_points[2 * i + j] = pos_degree_to_dscrt(ptp->axis_pos[j]); }
                      trajectory_3_times[i] = ptp->time_msec + 100 * (ptraj->start_delta);
                  }
                  else
                  {
                      for(int j = 0; j < 2; ++j)
                      { trajectory_3_points[2 * i + j] = trajectory_3_points[2 * (i - 1) + j]; }
                      trajectory_3_times[i] = trajectory_3_times[i - 1] + MSEC_INTERVAL;
                  }
              }
              struct trajectory_point* ppoint = get_write_point(&shs.current_traj_data.log, 0);
              for(int j = 0; j < 2; ++j)
              { ppoint->axis_pos[j] = pos_dscrt_to_degree(current_drive_state.axis[j].current_position); }
              ppoint->time_msec = trajectory_msec;
              // for test putposes
              /* { */
              /*     const struct trajectory_point* cpoint = get_read_point(&shs.current_traj_data.trajectory, 0); */
              /*     ppoint->time_msec = cpoint->time_msec; */
              /*     for(int j = 0; j < 2; ++j) */
              /*         ppoint->axis_pos[j] = cpoint->axis_pos[j]; */
              /* } */
          }
      }
      if(current_drive_state.operation_mode != mTracking)
          break;
      case mTracking:
      {
          // check if we should move to next trajectory point
          if(trajectory_msec > trajectory_3_times[1])
          {
              ++shs.current_traj_data.trajectory_cursor;
              struct trajectory_point* ppoint = get_next_point(&shs.current_traj_data.log);
              if(ppoint)
              {
                  for(int j = 0; j < 2; ++j)
                  { ppoint->axis_pos[j] = pos_dscrt_to_degree(current_drive_state.axis[j].current_position); }
                  ppoint->time_msec = trajectory_msec;
              }
              // for test purposes
              /* { */
              /*     const struct trajectory_point* cpoint = get_read_point(&shs.current_traj_data.trajectory, */
              /*                                                     shs.current_traj_data.trajectory_cursor); */
              /*     ppoint->time_msec = cpoint->time_msec; */
              /*     for(int j = 0; j < 2; ++j) */
              /*         ppoint->axis_pos[j] = cpoint->axis_pos[j]; */
              /* } */
              if(shs.current_traj_data.trajectory_cursor >= get_current_size(&shs.current_traj_data.trajectory))
              {
                  // we have reached the end of trajectory; we should stop
                  for(int i = 0; i < 2; ++i)
                      current_drive_state.axis[i].stopped = true;
                  current_drive_state.operation_mode = mIdle;
                  print_message(CONTROL_MESSAGE, "Stopping trajectory following: length: %d;\n",
                                shs.current_traj_data.trajectory.length);
                  print_message(CONTROL_MESSAGE, " real size: %d; log size: %d.\n",
                                get_current_size(&shs.current_traj_data.trajectory),
                                get_current_size(&shs.current_traj_data.log));
                  break;
              }
              // shift points data
              memmove(trajectory_3_points, trajectory_3_points + 2, sizeof(int32_t) * 2 * 2);
              memmove(trajectory_3_times, trajectory_3_times + 1, sizeof(int32_t) * 2);
              // get next point from the trajectory
              const struct trajectory_point* ptp = get_read_point(&shs.current_traj_data.trajectory,
                                                                  shs.current_traj_data.trajectory_cursor + 2);
              if(ptp != NULL)
              {
                  for(int j = 0; j < 2; ++j)
                  { trajectory_3_points[2 * 2 + j] = pos_degree_to_dscrt(ptp->axis_pos[j]); }
                  trajectory_3_times[2] = ptp->time_msec + 100 * shs.current_traj_data.trajectory.start_delta;
              }
              else
              {
                  for(int j = 0; j < 2; ++j)
                  { trajectory_3_points[2 * 2 + j] = trajectory_3_points[2 * 1 + j]; }
                  trajectory_3_times[2] = trajectory_3_times[1] + MSEC_INTERVAL;
              }
          }
          // prepare data
          perform_tracking(trajectory_3_points, trajectory_3_times, trajectory_msec, &current_drive_state);
      }
      break;
      }
      // is that the reason?
#ifndef DISABLE_LIMITS
      apply_limits(&current_drive_state, program_limits_pos, program_limits_neg);
#endif
      if((current_drive_state.operation_mode != mWaitStartTime)
         && (current_drive_state.operation_mode != mIdle)
         && (current_drive_state.operation_mode != mTracking))
      for(int i = 0; i < 2; ++i)
      {
          if(current_drive_state.axis[i].real_speed < FLT_MIN)
          {
              if(!current_drive_state.axis[i].stopped)
              {
                  print_message(ACTIVE_MESSAGE, "Axis %d stop counter: %d. Zprivod: %d, stop boundary: %d\n",
                                i + 1, stop_counters[i], abs(current_drive_state.axis[i].zprivod),
                                5 * current_parameters.K1[i]);
                  stop_counters[i]++;
                  if(stop_counters[i] >= stop_count)
                      current_drive_state.axis[i].stopped = true;
                  
              }
          }
          else
          {
              current_drive_state.axis[i].stopped = false;
              stop_counters[i] = 0;
          }
          if(current_drive_state.axis[i].stopped)
          {
              current_drive_state.axis[i].zprivod = 0;
          }
      }
      if(packet_recieved)
      {
          for(int i = 0; i < 2; ++i)
          {
              compute_real_speed(&current_drive_state.axis[i], trajectory_msec);
          }
      }
      if(packet_recieved)
         bup_interaction(&current_drive_state, &command, &alarm_counter, data->afd, inbuffer2, sizeof(inbuffer2));
      memset(inbuffer2, 0, sizeof(inbuffer2));
      gettimeofday(&end_op, NULL);
      print_message(LOG_MESSAGE, "Packet processed for %d microseconds\n",
                    end_op.tv_usec > begin_op.tv_usec?
                    end_op.tv_usec - begin_op.tv_usec:
                    end_op.tv_usec + 1000000 - begin_op.tv_usec);
            

      update_inner_stat(&current_drive_state);
   }
}

void update_inner_stat(const struct drive_states* pstate)
{
    if(!pthread_mutex_trylock(&shs.statelock))
    {
        memcpy((void*)&shs.current_state[0], pstate, sizeof(*pstate));
        shs.newstate = 1;
        pthread_mutex_unlock(&shs.statelock); 
    }
}
