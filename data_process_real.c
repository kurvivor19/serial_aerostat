#include "operations.h"
#include "bupprotocol.h"
#include "sensorprotocol.h"
#include "display.h"
#include "utils.h"

#include <stdlib.h>
#include <stddef.h>
#include <stdio.h>
#include <termios.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>

extern volatile sig_atomic_t input;

extern volatile sig_atomic_t done;

extern struct all_parameters current_parameters;

#ifndef AZIMUTH_INDEX
#define AZIMUTH_INDEX 2
#endif

/* #undef LOGPRINT */
/* #define LOGPRINT if(0) print_log_message */

bool parse_input_data(int fd, struct drive_states* pstate, int* counter, unsigned char* inbuffer, size_t buffer_size)
{
    // cursors for parsing data with recover from misalignment
    static size_t offset = 0, misalign = 0;
    size_t packet_start = 0;
    int res = 0;
    // azimuth  rotation borders
    const int32_t plus_180 = pos_degree_to_dscrt(180.0),
        plus_90 = pos_degree_to_dscrt(90.0),
        minus_180 = pos_degree_to_dscrt(-180.0),
        minus_90 = pos_degree_to_dscrt(-90.0);
    fd_set active_fd;
    FD_ZERO(&active_fd);
    FD_SET(fd, &active_fd);

    while(!shs.done)
    {
        struct timeval wait_interval = {0, 500};
        res = select(FD_SETSIZE, &active_fd, NULL, NULL, &wait_interval);
        if(res > 0 && FD_ISSET(fd, &active_fd))
        {
            // read all avaiable data - for non-blocking file descriptors
            // res = read(fd, inbuffer + offset, buffer_size - offset);
            // read only needed data - for blocking file descriptors
            res = read(fd, inbuffer + offset,
                       packet_start + sizeof(struct sensors_data_block) - offset);

            LOGPRINT("Recieved %d bytes on data port.\n", res);
            if(res)
                LOGPRINT("Data recieved: %s\n", make_hex_string((unsigned char*)(inbuffer + offset), res));
            offset += res;
            *counter += res;
            packet_start = 0;
            while((offset - packet_start) >= sizeof(struct sensors_data_block) && !shs.done)
            {
                // TODO: validate data
                misalign = validate_sensor_data(inbuffer + packet_start, offset - packet_start);
                if(!misalign)
                {
                    // validation succeed
                    // update known state
                    struct sensors_data_block* pindata = (struct sensors_data_block*)(inbuffer + packet_start);
                    pstate->flag = pindata->data.status;
                    for(int i = 0; i < 2; ++i)
                        pstate->axis[i].current_position = current_parameters.SensorSigns[i] * pindata->data.position[i];

                    // check azimuth axis flags
                    if(pindata->data.status & AZIMUTH_POSITIVE)
                    {
                        if(pstate->axis[AZIMUTH_INDEX].current_position < 0)
                        {
                            pstate->axis[AZIMUTH_INDEX].current_position = (pstate->axis[AZIMUTH_INDEX].current_position - minus_180) + plus_180;
                        }
                    }
                    else
                    {
                        if(pindata->data.status & AZIMUTH_NEGATIVE)
                        {
                            if(pstate->axis[AZIMUTH_INDEX].current_position > 0)
                            {
                                pstate->axis[AZIMUTH_INDEX].current_position = (pstate->axis[AZIMUTH_INDEX].current_position - plus_180) + minus_180;
                            }
                        }
                    }
                    // LOGPRINT("Packet recieved: %s\n", make_hex_string((unsigned char*)(pindata), sizeof(*pindata)));
                    // lock status data

                    // echo if able
                    memmove(inbuffer, inbuffer + packet_start + sizeof(struct sensors_data_block),
                            offset - packet_start - sizeof(struct sensors_data_block));
                    offset -= packet_start + sizeof(struct sensors_data_block);
                    shs.input = offset > 0;
                    return true;
                }
                else
                {
                    LOGPRINT("Packet misaligned by %d\n", misalign);
                    // LOGPRINT("Misaligned data: %s\n",
                    //                make_hex_string((unsigned char*)(inbuffer + packet_start), sizeof(struct sensors_data_block)));
                    // validation failed
                    packet_start += misalign;
                }
            }
            // currently read data are all insufficient
            if(packet_start >= buffer_size - sizeof(struct sensors_data_block) - 1)
            {
                // all data are discarded
                offset = 0;
                packet_start = 0;
            }
        }
        else
        {
            LOGPRINT("Timeout on data port.");
        }
        FD_SET(fd, &active_fd);
    }
    return false;
}
    
/*     do */
/*     { */
/*         res = read(fd, inbuffer + offset, buffer_size - offset); */
/*         // only valid input is processed */
/*         if(res <= 0) continue; */

/*         // LOGPRINT("Recieved %d bytes on active port.\n%s ...\n", */
/*         //               res, make_hex_string((unsigned char*)inbuffer + offset, res>5?5:res)); */

/*         LOGPRINT("Recieved %d bytes on data port.\n", res); */

/*         offset += res; */
/*         *counter += res; */
/*         while((offset - packet_start) >= sizeof(struct sensors_data_block)) */
/*         { */
/*             // TODO: validate data */
/*             misalign = validate_sensor_data(inbuffer + packet_start, offset - packet_start); */
/*             if(!misalign) */
/*             { */
/*                 // validation succeed */
/*                 // update known state */
/*                 struct sensors_data_block* pindata = (struct sensors_data_block*)(inbuffer + packet_start); */
/*                 pstate->flag = pindata->data.status; */
/*                 for(int i = 0; i < 3; (pstate->axis[i].current_position = pindata->data.position[i]), ++i); */
/*                 // LOGPRINT("Packet recieved: %s\n", make_hex_string((unsigned char*)(pindata), sizeof(*pindata))); */
/*                 // lock status data */
/*                 update_inner_stat(pstate); */

/*                 // echo if able */
/*                 if(efd != -1) */
/*                 { */
/* #if defined COUNTER */
/*                     char buffer[4]; */
/*                     hexprint_s((unsigned char*)&pindata->data.packet_counter, */
/*                                1, buffer); */
/*                     buffer[2] = ' '; */
/*                     buffer[3] = '\0'; */
/*                     write(efd, buffer, 4); */
/* #endif */
/*                 } */
/*                 memmove(inbuffer, inbuffer + packet_start + sizeof(struct sensors_data_block), */
/*                          offset - packet_start - sizeof(struct sensors_data_block)); */
/*                 offset -= packet_start + sizeof(struct sensors_data_block); */
/*                 // tcflush(fd, TCIFLUSH); */
/*                 return true; */
/*             } */
/*             else */
/*             { */
/*                 LOGPRINT("Packet misaligned by %d\n", misalign); */
/*                 // LOGPRINT("Misaligned data: %s\n", */
/*                 //                make_hex_string((unsigned char*)(inbuffer + packet_start), sizeof(struct sensors_data_block))); */
/*                 // validation failed */
/*                 packet_start += misalign; */
/*             } */
/*         } */
/*         // currently read data are all insufficient */
/*         if(packet_start >= offset) */
/*         { */
/*             // all data are discarded */
/*             offset = 0; */
/*         } */
/*         else */
/*         { */
/*             memmove(inbuffer, inbuffer + packet_start, offset - packet_start); */
/*             offset -= packet_start; */
/*         } */
/*         packet_start = 0; */
/*     } while(res > 0); */
/*     input = 0; */
/*     return false; */
