/** \file utils.h
 * some utilitary functions
 */

#ifndef _UTILS_H_
#define _UTILS_H_

#include <stddef.h>
#include <stdint.h>

/** Calculate dallas maxim control sum
 * \param[in] start pointer to first controlled byte
 * \param len number of bytes to calculate sum for
 * \returns crc
 */
uint8_t calculate_dallas(const uint8_t* start, size_t len);

/** Calculate crc8 control sum
 * \note implemented via lookup table
 * \param[in] start pointer to first controlled byte
 * \param len number of bytes to calculate sum for
 * \returns crc
 */
uint8_t calculate_crc8(const uint8_t* start, size_t len);

/** Print bytes in hex format into buffer
 * \param[in] data data to print
 * \param len number of bytes to print
 * \param[out] res buffer where resulting string goes
 * must be of size len*3 at least
 */
void hexprint_s(const unsigned char* data, size_t len, char* res);

/** Prepare constants used for conversion between degrees/descreets */
void init_conversion_routines();

double pos_dscrt_to_degree(int32_t dscr);

int32_t pos_degree_to_dscrt(double deg);

double spd_dscrt_to_degree(int32_t dscr);

int32_t spd_degree_to_dscrt(double deg);

static inline int sign(int val)
{
    return (0 < val) - (val < 0);
}

static inline int fsign(float val)
{
    return (0 < val) - (val < 0);
}
#endif // _UTILS_H_
