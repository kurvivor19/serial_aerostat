#include "sensorprotocol.h"
#include "utils.h"

size_t validate_sensor_data(const uint8_t* pblock, size_t buffer_size)
{
    if(buffer_size >= sizeof(struct sensors_data_block))
    {
        const struct sensors_data_block* pdata = (const struct sensors_data_block*)pblock;
        if(pdata->blockstart == sensor_block_start)
        {
            if(pdata->blockend == sensor_block_end)
            {
                uint8_t crc = calculate_dallas(pblock + 1, sizeof(struct sensors_data));
                if(crc == pdata->crc)
                    return 0;
            }
        }
    }
    size_t res;
    for(res = 1; res < buffer_size && sensor_block_start != pblock[res]; ++res);
    return res;
}
