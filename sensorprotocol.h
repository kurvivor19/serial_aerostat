/** \file sensorprotocol.h
 * Contains structures and enums of the protocol
 * used to recieve data from sensors block
 */

#ifndef _SENSORPROTOCOL_H_
#define _SENSORPROTOCOL_H_

#include <stdint.h>
#include <string.h>

enum __attribute__ ((packed)) endpoints
{
    DRIVE1_ENDPOINT_N = 0x01,
    DRIVE1_ENDPOINT_P = 0x02,
    DRIVE2_ENDPOINT_N = 0x04,
    DRIVE2_ENDPOINT_P = 0x08,
    DRIVE3_ENDPOINT_N = 0x10,
    DRIVE3_ENDPOINT_P = 0x20,
    AZIMUTH_POSITIVE = 0x40,
    AZIMUTH_NEGATIVE = 0x80
};

static const int left_endpoints[] = {
    DRIVE1_ENDPOINT_N,
    DRIVE2_ENDPOINT_N,
    DRIVE3_ENDPOINT_N
};

static const int right_endpoints[] = {
    DRIVE1_ENDPOINT_P,
    DRIVE2_ENDPOINT_P,
    DRIVE3_ENDPOINT_P
};

//#define COUNTER

/** data on drive endpoints and absolute sensor positions
*/
struct __attribute__ ((packed)) sensors_data
{
#if defined COUNTER
    uint8_t packet_counter;
#endif
    /// Some combination of \see endpoints flags
    uint8_t status;
    /// Signed integer drive positions
    int16_t position[3];
};

static const uint8_t sensor_block_start = 0xAA;
static const uint8_t sensor_block_end = 0x55;

struct __attribute__ ((packed)) sensors_data_block
{
    uint8_t blockstart;
    struct sensors_data data;
    uint8_t crc;
    uint8_t blockend;
};


/** Validate data as a data from sensors
 * Reinterpret data as a \see sensors_data_block
 * Valid structures must have defined constants in the first and last bytes
 * and correct crc. Determines misalignment, if any
 * \param[in] pblock incoming data
 * \param buffer_size size of buffer being processed
 * \returns 0 for valid data
 * \returns misalignment up to buffer size
 */
size_t validate_sensor_data(const uint8_t* pblock, size_t buffer_size);

#endif // _SENSORPROTOCOL_H_
