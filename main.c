/** \file main.c
 * \brief Main function of serialtest program
 * Contains main function of serialtest program. Performs
 * parsing of command line and starts processing of data
 * from serial terminal in requested mode. On parameters
 * see more in help output of the program, accessed with:
 * > serialtest -h
 * or
 * > serialtest --help
 */

#include "initialisation.h"
#include "trkprotocol.h"
#include "display.h"

#include <stdio.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <linux/serial.h>

#include <pthread.h>
#include <signal.h>
#include <stdlib.h>

/** Main loop function
 * Loops with frequency given by pmode->frequency
 * loop is shut down by SIGTERM or SIGINT
 * \param ptty_active file descriptor of active port
 * \param pport_control data on TCP connection
 * \param ptty_echo file descriptor of echo port
 * \param ptty_data file descriptor of data port
 * \param ptty_termo file descriptor of termo port
 * \param pmode how the program should run
 * \returns 0 if the function is completed
 * \returns 1 if error is encountered
 */
int operate(int ptty_active, struct tcp_config* pport_control, int ptty_echo,
            int ptty_data, int ptty_termo, struct workmode* pmode);

/** Open port and set its parameters
 * \param pconfig serial port name and configuration
 * \param verbose whether to print what is going on to stdout or not
 * \param[out] poldconfig old terminal configuration to be restored later
 * \param[in] async true if port should be opened as asynchronous
 * \returns open port's file id
 * \returns -1 if we did not succeed
 * \note in that case port is also closed
 */
int prepare_port(const struct serial_config* pconfig, struct termios* poldconfig, bool async);

/** Change port type for given open port
 * Port type is one of UARTs or 0 for unknown type
 * Since moxa driver only acknoledges UART16550A or something else,
 * we switch between that and unknown.
 * \param fd file descriptor of the port
 * \param ptype type of UART
 * \returns -1 if we did not succeed
 */
int set_port_type(int fd, int ptype);

/** Initialise and bind to socket with given adress
 * Socket is put into the input struct
 * \param[inout] pport structure describing socket
 * \returns socket id on success (same one that is also put in structure)
 * \returns -1 on failure
 */
int prepare_socket(struct tcp_config* pport);

time_t day_start, day_end;

void lastalarm(int sugnum)
{
    // exit
    exit(EXIT_FAILURE);
}

int main(int argc, char* argv[])
{
    // determine current day start and end
    time_t now = time(NULL);
    struct tm* today = gmtime(&now);
    today->tm_sec = 0;
    today->tm_min = 0;
    today->tm_hour = 0;
    day_start = mktime(today);
    day_end = day_start + day_len;

    struct termios active_config_old, control_config_old, echo_config_old,
        data_config_old, termo_config_old;
    struct workmode mode;
    init_default_workmode(&mode);    
    int atty = -1, ctty = -1, etty = -1, dtty = -1, ttty = -1; // terminal desciptors
    if(!process_command_line(argc, argv, &mode))
    {
        usage();
        return 1;
    }
    init_display(mode.rich, !mode.mute);
    load_settings(mode.ini_file, !mode.mute);
    print_log_message("KZ2D value: %f\n", current_parameters.KZ2D);

    finish_port_init(&mode);
    if(!strcmp(mode.active_port.device, mode.data_port.device))
    {
        print_log_message("Active and data terminal ports must be different\n");
	refresh_display();
	deinit_display();
	return 1;
    }
    ctty = prepare_socket(&mode.control_port);
    atty = prepare_port(&mode.active_port, &active_config_old, true);

    dtty = prepare_port(&mode.data_port, &data_config_old, false);
    if(0 == set_port_type(dtty, PORT_UNKNOWN))
    {
        print_log_message("Data port set to use 1-byte buffer\n");
    }
    if(0 == set_port_type(atty, PORT_16450))
    {
        print_log_message("Active port set to use 1-byte buffer\n");
    }
    if(atty != -1 && dtty != -1 && ctty != -1)
    {
        mode.control_port.socket = ctty;
        if(strlen(mode.echo_port.device) > 0)
        {
            etty = prepare_port(&mode.echo_port, &echo_config_old, false);
        }
        if(strlen(mode.termo_port.device) > 0)
        {
            ttty = prepare_port(&mode.termo_port, &termo_config_old, false);
        }
        operate(atty, &mode.control_port, etty, dtty, ttty, &mode);
    }

    // ensure we always exit
    struct sigaction action = {lastalarm, 0, 0, 0, 0};
    sigaction(SIGALRM, &action, NULL);
    alarm(10);

    refresh_display();
    deinit_display();
    printf("Saving settings...\n");
    save_settings(mode.ini_file, !mode.mute);

    if(atty != -1)
    {
        printf("Restoring type of \'%s\'\n", mode.active_port.device);
        set_port_type(atty, PORT_16550A);
        printf("Restoring settings of \'%s\'\n", mode.active_port.device);
        restore_attribs(atty, &active_config_old);
        printf("Closing serial device \'%s\'\n", mode.active_port.device);
	close(atty);
    }
    if(ctty != -1)
    {
        if(mode.control_port.file != -1)
        {
            printf("Closing existing connection\n");
            close(mode.control_port.file);
        }
        printf("Closing socket\n");
        close(ctty);
        freeaddrinfo(mode.control_port.addr_info);
    }
    if(dtty != -1)
    {
        printf("Restoring type of \'%s\'\n", mode.data_port.device);
            set_port_type(dtty, PORT_16550A);
        printf("Restoring settings of \'%s\'\n", mode.data_port.device);
        restore_attribs(dtty, &data_config_old);
        printf("Closing serial device \'%s\'\n", mode.data_port.device);
        close(dtty);
    }
    if(etty != -1)
    {
        printf("Restoring settings of \'%s\'\n", mode.echo_port.device);
        restore_attribs(etty, &echo_config_old);
        printf("Closing serial device \'%s\'\n", mode.echo_port.device);
        close(etty);
    }
    if(ttty != -1)
    {
        printf("Restoring settings of \'%s\'\n", mode.termo_port.device);
        restore_attribs(ttty, &termo_config_old);
        printf("Closing serial device \'%s\'\n", mode.termo_port.device);
        close(ttty);
    }
    return 0;
}

int prepare_port(const struct serial_config* pconfig, struct termios* poldconfig, bool async)
{
    struct termios config;
    LOGPRINT("Will operate on device \'%s\'\n", pconfig->device);
    LOGPRINT("Opening serial device\n");
    int flag = O_RDWR | O_NOCTTY | ASYNC_LOW_LATENCY;
    int fd = open(pconfig->device, async? flag | O_NONBLOCK: flag);
    if(fd == -1)
    {
        print_log_message("Failed to open terminal file.\nError #%d (%s)\n",
               errno, strerror(errno));
	refresh_display();
        return fd;
    }

    LOGPRINT("Retrieving terminal attributes\n");
    if(!!tcgetattr(fd, &config))
    {
        print_log_message("Failed to retrieve terminal attributes.\nError #%d(%s)\n",
               errno, strerror(errno));
	close(fd);
	fd = -1;
	refresh_display();
	return fd;
    }
    else
    {
        if(poldconfig != NULL)
            memcpy(poldconfig, &config, sizeof(struct termios));
	
        LOGPRINT("Setting terminal attributes\n");
        set_attribs(fd, &config, pconfig);
        tcgetattr(fd, &config);
        display_attribs(&config);
	refresh_display();
	return fd;
    }
}

int set_port_type(int fd, int ptype)
{
    struct serial_struct temp;
    int res = ioctl(fd, TIOCGSERIAL, &temp);
    if(!!res)
    {
        print_log_message("Failed to retrieve serial_struct.\n");
        return res;
    }
    temp.type = ptype;
    res = ioctl(fd, TIOCSSERIAL, &temp);
    if(!!res)
    {
        print_log_message("Failed to set serial_struct.\n");
        return res;        
    }
    return res;
}

int prepare_socket(struct tcp_config* pport)
{
    int res = -1, sfd = -1;
    int optval = 1;
    if(!pport)
    {
        print_log_message("Wrong parameters supplied.\n");
        return res;
    }
    pport->socket = -1;
    pport->file = -1;
    
    struct addrinfo *rp;
    for(rp = pport->addr_info; rp != NULL; rp = rp->ai_next)
    {
        res = -1;
        sfd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
        if(sfd == -1)
            continue;

        res = fcntl(sfd, F_SETFL, O_NONBLOCK);
        if(res != -1)
        {
            res = setsockopt(sfd, SOL_SOCKET, SO_REUSEADDR,
                             (void*)&optval, sizeof(optval));
            if(res != -1)
            {
                res = bind(sfd, rp->ai_addr, rp->ai_addrlen);
                if(res == 0)
                {
                    res = listen(sfd, 1);
                    if(res == 0)
                    {
                        pport->socket = sfd;
                        // display message on adress
                        char hostname[50], servicename[50];
                        getnameinfo(rp->ai_addr, sizeof(*rp->ai_addr),
                                    hostname, 50, servicename, 50,
                                    NI_NUMERICHOST | NI_NUMERICSERV);
                        print_log_message("Succesfully bound to adress %s: %s\n", hostname, servicename);
                        break;
                    }
                    else
                    {
                        print_log_message("Failed to start listening; error code %d\n", res);
                    }
                }
                else
                {
                    print_log_message("Failed to bind; error code %d\n", res);
                }
            }
            else
            {
                print_log_message("Failed to set REUSE ADDRESS socket option\n");
            }
        }
        else
        {
            print_log_message("Failed to set socket to non-blocking\n");
        }
        close(sfd);
        sfd = -1;
    }
    if(sfd == -1)
    {
        freeaddrinfo(pport->addr_info);
    }
    return sfd;
}
