# This is a test project for transmitting data over serial port from Linux
# phony targets
.PHONY: clean source tags

# honoring code conventions
SHELL = /bin/sh

CFLAGS = -std=c99 -Werror -D_GNU_SOURCE -DNDEBUG -DAZIMUTH_INDEX=0 -g
# CFLAGS = -std=c99 -D_GNU_SOURCE -DNDEBUG -DBUP_STATUS -g
SOURCEDIR = $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
BUILDDIR = `pwd`

OBJECTS = main.o initialisation.o operations.o trajectory.o utils.o display.o\
           inifile.o sensor_thread.o bupprotocol.o sensorprotocol.o termo_thread.o

all: program source
	echo 'Build succeed!'

program: ${OBJECTS} data_process_real.o
	${CC} ${CFLAGS} ${OBJECTS} data_process_real.o -lpthread -lncurses -lm -o aerostat

# we will probably need dummy later
# dummy: ${OBJECTS} data_process_dummy.o
# 	${CC} ${CFLAGS} ${OBJECTS} data_process_dummy.o -lpthread -lncurses -lm -o serialdummy

.o:
	${CC} ${CFLAGS} -c ${SOURCEDIR}/$.c -o $@

source:
	tar -cz -f source.tar.gz -C ${SOURCEDIR} *.c *.h

clean:
	echo 'Removing temp files'
	rm -f ${SOURCEDIR}/*~
	rm -f ${SOURCEDIR}/#*
	echo 'Removing old build'
	rm -f aerostat
	rm -f *.o
	rm -f *.gz
	echo 'Cleanup complete'

tags:
	rm -f TAGS
	etags --declarations *.c *.h
